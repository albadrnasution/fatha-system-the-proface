<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Face extends Public_Controller
{
    private $data = array();

    /**
     * The constructor
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->lang->load('face');
        $this->lang->load('buttons');
        $this->load->helper('my_file');
        $this->load->driver('Streams');
        //$this->template->append_css('module::faq.css');
    }
     /**
     * List all FAQs
     *
     * We are using the Streams API to grab
     * data from the faqs database. It handles
     * pagination as well.
     *
     * @access	public
     * @return	void
     */
    public function index()
    {	
		if (!$this->current_user) redirect('users/login');
	
        // Get stream entries
        $params = array();
        $params['stream'] = 'face';
        $params['namespace'] = 'aisl';
        $params['where'] = "owner={$this->current_user->id}";
        $data = $this->streams->entries->get_entries($params);
		
		// Get Facebook photos entries
        $params = array();
        $params['stream'] = 'sn_photo';
        $params['namespace'] = 'aisl';
        $params['where'] = "photo_owner={$this->current_user->id}&&photo_sn='fb'";
        $params['sort'] = "asc";
        $data['fb_images'] = $this->streams->entries->get_entries($params);
        
		
		
		$this->template
			->append_css('module::jquery-ui/jquery-ui.css')
			->append_js('module::jquery-ui/jquery-ui.js')
			->append_css('module::colorbox/colorbox.css')
			->append_js('module::colorbox/jquery.colorbox.js')
			->append_js('module::imagesloaded.js')
			->append_js('module::packery.js');
		$this->template->title(lang('face:list'));
        $this->template->build('list_face', $data);
    }
	
	public function add()
    {
        // Load everything we need
        $this->load->library('streams_core/Fields');

        $stream = $this->streams->streams->get_stream('face', 'aisl');
        $namespace = 'aisl';
        $mode = 'new'; //'new', 'edit'
        $entry = null;
        $plugin = false;
        $recaptcha = false;
        $extra = array(
            'return' => 'student/face/examine/-id-',
            'success_message' => lang('event:events:submit_success'),
            'failure_message' => lang('event:events:submit_failure'),
            'title' => 'lang:event:events:new',
            //'required' => '<span>*</span>',
            //'email_notification' => false,
        );
        $skips = array();
        $defaults = array();

        // Get our field form elements.
        $data['fields'] = $this->fields->build_form($stream, $mode, $entry, $plugin, $recaptcha, $skips, $extra, $defaults);
        $data['stream'] = $stream;
        $data['mode'] = $mode;
        $data['return'] = $extra['return'];
        $data['entry'] = $entry;

        // Make input_slug as the array key
        // in order to ease form templating.
        if (is_array($data['fields'])) {
            $fields_arr = array();
            foreach ($data['fields'] as $field) {
                $fields_arr[$field['input_slug']] = $field;
            }
            $data['fields'] = $fields_arr;
		}
		
        // Build the form page.
        $this->template->title(lang('event:events:new'));
        $this->template->build('add_face', $data);
    }
	
	
    public function edit($id = 0)
    {
        // Load everything we need
        $this->load->library('streams_core/Fields');

        $stream = $this->streams->streams->get_stream('face', 'aisl');
        $namespace = 'aisl';
        $mode = 'edit'; //'new', 'edit'
		if( ! $entry = $this->streams->entries->get_entry($id, $stream, $namespace)){
			$this->log_error('invalid_row', 'form');
		}
        $plugin = false;
        $recaptcha = false;
        $extra = array(
            'return' => 'student/face/',
            'success_message' => lang('event:events:submit_success'),
            'failure_message' => lang('event:events:submit_failure'),
            'title' => 'lang:event:events:new',
            //'required' => '<span>*</span>',
            //'email_notification' => false,
        );
        $skips = array();
        $defaults = array();

        // Get our field form elements.
        $data['fields'] = $this->fields->build_form($stream, $mode, $entry, $plugin, $recaptcha, $skips, $extra, $defaults);
        $data['stream'] = $stream;
        $data['mode'] = $mode;
        $data['return'] = $extra['return'];
        $data['entry'] = $entry;

        // Make input_slug as the array key
        // in order to ease form templating.
        if (is_array($data['fields'])) {
            $fields_arr = array();
            foreach ($data['fields'] as $field) {
                $fields_arr[$field['input_slug']] = $field;
            }
            $data['fields'] = $fields_arr;
		}
		
        // Build the form page.
        $this->template->title(lang('event:events:new'));
        $this->template->build('edit_face', $data);
    }
	
	public function delete($id = 0)
    {
        $this->streams->entries->delete_entry($id, 'face', 'aisl');
        $this->session->set_flashdata('error', lang('xxxxxxxxx'));
 
        redirect('student/face/');
    }
	public function deletefb($id = 0)
    {
		$this->load->model('face_m');
		
        // delete the entry from fb and the file
        $this->face_m->deleteFbFace($id);
		$this->session->set_flashdata('error', lang('Face is deleted'));
		
		// back to face list
        redirect('student/face/');
    }
	public function deletefball()
    {
		$params = array(
			'stream'        => 'sn_photo',
			'namespace'     => 'aisl',
			'where'			=> "photo_owner={$this->current_user->id}&&photo_sn='fb'"
		);
        $fbimages = $this->streams->entries->get_entries($params);
		
		if ($fbimages["total"] > 0){
			// reiterate through to delete all
			$faceIds = array();
			foreach ($fbimages["entries"] as $fbi){
				$faceIds[] = $fbi['id'];
			}
			$faceIds_json = json_encode($faceIds);
			echo $faceIds_json;
		}
	}
    
	public function update($id = 0){
		//load helper
        $this->load->helper('my_file');
		
		if (!empty($_POST)){
			$slug = $_POST["slug"];
			// Convert received info from json to object
			$received_info = json_decode($_POST["info"]);
			
			if ($slug == "fb"){ 
				/* Facebook Images: table is sn_photo */
				// Get facebook id of image
				$img_fbid = $received_info->img_id;
				
				// Get face based on facebook image_id
				$params['stream'] = 'sn_photo';
				$params['namespace'] = 'aisl';
				$params['where'] = "photo_owner={$this->current_user->id}&&photo_sn='{$slug}'&&photo_id='{$img_fbid}'";
				$data = $this->streams->entries->get_entries($params);
				
				if (!empty($data['entries'][0])){
					// Continue update if the facebook image_id exist within database
					$face = $data['entries'][0];
			
					// Get json from column face_info
					$face_info_fromdb = $face['photo_info'];
					$face_info_fromdb = json_decode(utf8_kill_entity_decode($face_info_fromdb));
					// Update to new face_candidate
					$face_info_fromdb->candidates = $received_info->face_candidates;
					$face_info_fromdb->selection = $received_info->face_selection;
					// Compose update item
					$entry_data = array(
						'photo_info' => json_encode($face_info_fromdb)
					);
					// Update the SN_Photo table
					$return = $this->streams->entries->update_entry($face['id'], $entry_data,'sn_photo', 'aisl');
					// Return something
					echo $return;
				}
				
			}else{			 
				/* Uploaded Images: table is `face` */
				// Compose face info
				$face_info = (object) array(
					'candidates' => $received_info->face_candidates,
					'selection'  => -1
				);
				// Compose update item
				$entry_data = array(
					'face_info' => json_encode($face_info)
				);
				// Update the Face table
				$return = $this->streams->entries->update_entry($id, $entry_data,'face', 'aisl');
				// Return something
				echo $return;
			}
		}
	}
	
	public function select($id = 0, $face_candidate = -1){
		//load helper
        $this->load->helper('my_file');
		
		//get face
		$face = $this->streams->entries->get_entry($id, 'face', 'aisl');
		//get json from column face_info
		$json_info = $face->face_info;
		$face_info = json_decode(utf8_kill_entity_decode($json_info));
		//update to new face_candidate
		$old_candidate = $face_info->selection;
		$face_info->selection = $face_candidate;
		
		$entry_data = array(
			'face_info' => json_encode($face_info)
		);
		$return = $this->streams->entries->update_entry($id, $entry_data,'face', 'aisl');
		echo $return;
		
		// Copy the physical file of face_candidate image to `face` folder under fu_photos.
		if ($face_candidate != -1 && $old_candidate != $face_candidate){
			//path for fileUploaded photo
			$fu_photos = FCPATH . "uploads/default/fu_photos/";
			
			//delete old candidate
			$old_candidate = $fu_photos . "face/" . $face->owner . "/i" . $face->id . "_c" . $old_candidate . ".jpg";
			unlink($old_candidate);
			
			//copy new
			$file = $fu_photos . "candidate/" . $face->owner . "/i" . $face->id . "_c" . $face_candidate . ".jpg";
			$newfile = $fu_photos . "face/" . $face->owner . "/i" . $face->id . "_c" . $face_candidate . ".jpg";
			//create directory in new destination if not present
			check_dir($newfile);
			//copy file to new
			if (!copy($file, $newfile)) {
				echo " Failed to copy $file...\n";
			}
		}
	}

	/** Send an image of 'face' stream to server and receive face area JSON from server.
	    Save json to database.
	*/
	public function examine($id = 0){
		$this->load->model('face_m');
        $this->config->load('config');
		
		// if valid id
		if ($id != 0){
			// Get image from file 
			$img = $this->streams->entries->get_entry($id, 'face', 'aisl');
			
			//var_dump($img);
			// Return string json to ServerFace
			$return_json = array(
				'source' => 'web',
				'request' => 'detect',
				'info' => array(
					'slug' => 'file_upload',
					'user' => $this->current_user->id,
					'uid' => $this->current_user->id,
					'iid' => $id,
					'picture' => "uploads/default/files/" . $img->face['filename'],
					'source' => $img->face['image'],
					'path' => $this->face_m->fusave_path,
					'x' => -1,
					'y' => -1,
				)
			);
			//$data['json']= json_encode($return_json);
			// Ask server for face detction and update
			$this->face_m->requestServerForDetection(json_encode($return_json));
			
			//get updated image
			$img = $this->streams->entries->get_entry($id, 'face', 'aisl');
			
			//prepare data for view
			$data['img_id'] = $id;
			$data['img_owner'] = $img->owner;
			$data['img_path'] = $img->face['image'];
			$data['img_info'] = (object) array(
				'width' => $img->face['width'], 
				'height'=> $img->face['height']
			);
			// Set face area candidates properties
			$data['face_cds'] = json_decode(utf8_kill_entity_decode($img->face_info));
			
			$this->template->title('');
			$this->template->build('detect_face', $data);
		}
	}
	
	
	/** Send an image of 'face' stream to server and receive face area JSON from server.
	    Save json to database.
	*/
	public function exclude($id = 0, $yesNo = 1){
		// if valid id
		if ($id != 0){
			// Get image from file 
			$face = $this->streams->entries->get_entry($id, 'sn_photo', 'aisl');
			// Get info
			$face_info_fromdb = $face->photo_info;
			$face_info_fromdb = json_decode(utf8_kill_entity_decode($face_info_fromdb));
			// Add additional parameter, exclude=yes
			if ($yesNo == 1){
				$face_info_fromdb->exclude = "yes";
			}else{
				$face_info_fromdb->exclude = "no";
			}
			// Compose update item
			$entry_data = array(
				'photo_info' => json_encode($face_info_fromdb)
			);
			// Update the SN_Photo table
			$return = $this->streams->entries->update_entry($id, $entry_data,'sn_photo', 'aisl');
			// Return something
			echo "Moving the image...";
			
			
			// Cut the physical file of face image to `exluded` folder under fb_photos.
			//path for fileUploaded photo
			$fb_photos = FCPATH . "uploads/default/fb_photos/";
			$normalPath  = $fb_photos . "face/"         . $face->photo_owner . "/" . $face->photo_id . ".jpg";
			$excludePath = $fb_photos . "face_exclude/" . $face->photo_owner . "/" . $face->photo_id . ".jpg";
			if ($yesNo == 1){
				//check destination
				check_dir($excludePath);
				//move
				if (file_exists($normalPath)) rename($normalPath, $excludePath);
			}else{
				//check destination
				check_dir($normalPath);
				//move
				if (file_exists($excludePath)) rename($excludePath, $normalPath);
			}
			
			
			// TODO: remove redirect below and apply ajax
			redirect('student/face/');
		}
	}

	
}


