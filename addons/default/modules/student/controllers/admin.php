<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 *
 * 
 * 
 * 
 * 
 *
 * 
 * 
 *
 * @author 		Albadr
 * @package 	AISL Student Face Manager
 */
class Admin extends Admin_Controller
{
    // This will set the active section tab
   protected $section = 'face';

    public function __construct()
    {
        parent::__construct();

        $this->lang->load('face');
        $this->load->driver('Streams');
    }
	
	/**
     * List all face images.
     *
     * @return	void
     */
	public function index()
    {
		//load helper
        $this->load->helper('my_file');
        // Get stream entries
        $params = array();
        $params['stream'] = 'face';
        $params['namespace'] = 'aisl';
        $params['order_by'] = 'owner';
        $data = $this->streams->entries->get_entries($params);
		
		$this->template
			->append_css('module::jquery-ui/jquery-ui.css')
			->append_js('module::jquery-ui/jquery-ui.js');
		$this->template->title(lang('face:list'));
        $this->template->build('admin/all_face', $data);
    }
	
	public function add_face()
    {
        $extra = array(
            'return' => 'admin/student',
            'success_message' => lang('faq:submit_success'),
            'failure_message' => lang('faq:submit_failure'),
            'title' => 'New Face',
         );

        $this->streams->cp->entry_form('face', 'aisl', 'new', null, true, $extra);
    }

    public function edit($id = 0)
    {
        $extra = array(
            'return' => 'admin/student',
            'success_message' => lang('Photo edited'),
            'failure_message' => lang('Failed to edit photo'),
            'title' => 'Edit'
        );

        $this->streams->cp->entry_form('face', 'aisl', 'edit', $id, true, $extra);
    }
	
	public function delete($id = 0)
    {
        $this->streams->entries->delete_entry($id, 'face', 'aisl');
        $this->session->set_flashdata('error', lang('xxxxxxxxx'));
 
        redirect('admin/student/');
    }
	
	public function view($id)
	{
		$this->load->helper('user');
	
		$this->load->model(array('users/user_m'));
		$data['student'] = $this->user_m->get(array('id' => $id));
		
		//fetch images
        $params = array();
        $params['stream'] = 'face';
        $params['namespace'] = 'aisl';
        $params['where'] = "owner={$id}";
        $data['faces'] = $this->streams->entries->get_entries($params);
		
		//fetch classes this user registered to 
        $params = array();
        $params['stream'] = 'attendance';
        $params['namespace'] = 'aisl';
        $params['where'] = "student={$id}";
        $data['classes'] = $this->streams->entries->get_entries($params);
		
		$this->template
			->append_css('module::jquery-ui/jquery-ui.css')
			->append_js('module::jquery-ui/jquery-ui.js');
		$this->template->title(lang('face:list'));
        $this->template->build('admin/view_student', $data);
	}
    
}

