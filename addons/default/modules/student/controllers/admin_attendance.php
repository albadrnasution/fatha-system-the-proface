<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 *
 * 
 * 
 * 
 * 
 *
 * 
 * 
 *
 * @author 		Albadr
 * @package 	AISL Student Face Manager
 */
class Admin_attendance extends Admin_Controller
{
    // This will set the active section tab
   protected $section = 'attendance';

    public function __construct()
    {
        parent::__construct();

        $this->lang->load('face');
        $this->load->driver('Streams');
    }
	
	/**
     * List all face images.
     *
     * @return	void
     */
	public function index()
    {
	    echo "Will show student-class relationship";
    }
	
	public function create()
    {
        $extra = array(
            'return' => 'student/admin/class',
            'success_message' => lang('faq:submit_success'),
            'failure_message' => lang('faq:submit_failure'),
            'title' => 'New Class',
         );

        $this->streams->cp->entry_form('attendance', 'aisl', 'new', null, true, $extra);
    }

    public function edit($id = 0, $class_id)
    {
		$return_url = 'student/admin/class/view/' . $class_id;
        $extra = array(
            'return' => $return_url,
            'success_message' => lang('faq:submit_success'),
            'failure_message' => lang('faq:submit_failure'),
            'title' => 'Edit Student Attendance',
         );

        $this->streams->cp->entry_form('attendance', 'aisl', 'edit', $id, true, $extra);
    }
	
	public function delete($id = 0)
    {
		$class_id = $this->input->get("cid");
        $this->streams->entries->delete_entry($id, 'attendance', 'aisl');
        $this->session->set_flashdata('error', lang('Error!'));
 
        redirect('student/admin/class/view/' . $class_id);
    }
	
	public function delete_teacher($id = 0)
    {
		$class_id = $this->input->get("cid");
        $this->streams->entries->delete_entry($id, 'teacher', 'aisl');
        $this->session->set_flashdata('error', lang('Error!'));
 
        redirect('student/admin/class/view/' . $class_id);
    }
    
}

