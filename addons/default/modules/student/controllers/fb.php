<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

// Autoload the required files
require_once( FCPATH . 'vendor/autoload.php' );
// added in v4.0.5
use Facebook\FacebookHttpable;
use Facebook\FacebookCurl;
use Facebook\FacebookCurlHttpClient;
//
use Facebook\FacebookSession;
use Facebook\FacebookRequest;
use Facebook\GraphUser;
use Facebook\FacebookRequestException;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\AccessToken;
use Facebook\FacebookJavaScriptLoginHelper;

class Fb extends Public_Controller
{
    private $data = array();
    private $fb_api_id;
    private $fb_api_sc;

    /**
     * The constructor
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->config->load('config');
        $this->lang->load('face');
        $this->lang->load('buttons');
        $this->load->driver('Streams');
        $this->load->library('session');
	
        $this->load->helper('my_file');
        $this->fb_api_id = $this->config->item('api_id', 'facebook');
        $this->fb_api_sc = $this->config->item('app_secret', 'facebook');
        FacebookSession::setDefaultApplication($this->fb_api_id, $this->fb_api_sc);
    }
    
    public function index(){
        if (!$this->current_user) redirect('users/login');
		$this->load->model('snconnection_m');
		
		
        /* Check whether already login to facebook or not*/
		$snc = $this->snconnection_m->findConnection_byWebpageID($this->current_user->id);
	
        if ($snc){ /* some connection is happened before, do connection with access token */
            $session_obj = $snc['sn_info'];
			//save access_token to session
			$this->session->set_userdata('fb_token', $session_obj->access_token);
			$this->session->set_userdata('fb_uid', $session_obj->user_id);
			
			$data['fb_connected'] = true;
			$data['fb_info'] = $this->get_me($session_obj->access_token);
			
			$user_permissions = $this->get_permission($session_obj->access_token);
			$photo_isGranted = $this->check_photos_permission($user_permissions);
			$data['photo_permission'] = $photo_isGranted;
			if ($photo_isGranted){
				//  Get facebook photos to be shown on this page
				$data['graphObject'] = $this->get_photos($session_obj->access_token);
			}
            
        }else{
            /* no history of connection, ask to allow connection */
			$data['fb_connected'] = false;
			$data['photo_permission'] = false;
			
        }
		$this->template->title(lang('sb:fb'));
		$this->template->build('fb_photo_snippet', $data);
    }
	
	private function get_permission($access_token){
		$url = 'https://graph.facebook.com/me/permissions?'.http_build_query(array(
			'access_token' => $access_token,
		));
		return json_decode(file_get_contents($url));		
	}
	
	private function get_photos($access_token, $params = array()){
		// make session object
		$session = new FacebookSession($access_token);
		//fetch user photos on facebook
		$request = new FacebookRequest($session, 'GET', '/me/photos', $params);
		$response = $request->execute();
		$graphObject = $response->getGraphObject();
		return $graphObject->asArray();
	}
	
	private function get_me($access_token){
		$request = new FacebookRequest(new FacebookSession($access_token), 'GET', '/me');
		$response = $request->execute();
		return $response->getGraphObject()->asArray();
	}
	
	private function check_photos_permission($user_permissions){
		$user_photos_granted = false;
		if ($user_permissions)
			foreach ($user_permissions->data as $up)
				if ($up->permission == "user_photos" && $up->status == "granted")
					$user_photos_granted = true;
		return $user_photos_granted;
	}
    
    public function authorized(){
		$this->load->model('snconnection_m');
        try {
          $helper = new FacebookRedirectLoginHelper(site_url('student/fb/authorized'));
          $session = $helper->getSessionFromRedirect();
        } catch( FacebookRequestException $ex ) {
          // When Facebook returns an error
        } catch( Exception $ex ) {
          // When validation fails or other local issues
			echo $ex;
        }
		
        if (isset($session)){
            //fetch user on facebook
            $request = new FacebookRequest( $session, 'GET', '/me');
            $response = $request->execute();
            // get response
            $graphObject = $response->getGraphObject()->asArray();
			
            //save access_token to session
            $this->session->set_userdata('fb_token', $session->getToken());
            $this->session->set_userdata('fb_uid', $graphObject["id"]);
			
			// check whether user has logged in to this webpage
			if (is_logged_in()){
				// Yes? Update connection
				$this->snconnection_m->updateConnection($this->current_user->id, $graphObject["id"], $session->getToken());
				redirect("my-profile");
			}else{
				// Not logged in? Log him in!
				$sn_conn = $this->snconnection_m->loginUser_withFacebookID($graphObject['id']);
				
				if (!$sn_conn)
					redirect("users/login");
				else
					redirect("student/fb");
			}
		}
		else if ($_GET['error']=='access_denied'){
			redirect("my-profile");
		}
	}
   
	public function authorize(){
        $helper = new FacebookRedirectLoginHelper( site_url('student/fb/authorized'));
		redirect($helper->getLoginUrl());
	}
	
	public function reauthorize(){
        $helper = new FacebookRedirectLoginHelper( site_url('student/fb/authorized'));
		$login = $helper->getLoginUrl(array('scope'=>'user_photos'));
		redirect($login."&auth_type=rerequest");
	}
	
	public function revoke(){
		$this->load->model('snconnection_m');
		$access_token = $this->session->userdata('fb_token');
        $fb_uid  = $this->session->userdata('fb_uid');
		
		$request = new FacebookRequest(new FacebookSession($access_token), 'DELETE', '/'. $fb_uid.'/permissions');
		$response = $request->execute();
		$ret = $response->getGraphObject()->asArray();
		
		if ($ret['success']){
			$this->snconnection_m->deleteConnection($this->current_user->id);
			redirect('student/fb');
		}else{
			echo "Error";
		}
	}
   
	/**
	*  Download all foto on facebook by procedure:
	*  1. Collect all photo links from Facebook GraphObject
	*  2. Create path
	*  3. For each photo link, save to path and database
	*  With this procedure, the saving take slow in the process 1 and until all photo is saved,
	*  the browser page will look like freezing (processing).
	* @ SUPER OBSOLETE now it is private
	*/
    private function hard_download(){
        if (!$this->current_user) redirect('users/login');
        
        
        $fb_token = $this->session->userdata('fb_token');
        if (isset($fb_token) && $fb_token != ""){
            //download all
            
            // FIRST REQUEST
            // graph api request for user data
            $params = array();
            if (isset($_GET["after"])) $params['after'] = $_GET["after"];
            if (isset($_GET["before"])) $params['before'] = $_GET["before"];
            
            //create facebook session
            $session = new FacebookSession($fb_token);
            //fetch user id on facebook
            $request = new FacebookRequest( $session, 'GET', '/me/photos', $params);
            $response = $request->execute();
            // get response
            $graphObject = $response->getGraphObject();
            $graphObject = $graphObject->asArray();
            $paging = $graphObject['paging'];
            
            // variable to save all facebook photo
            $all_fb_photos = array();
            $all_fb_photos[] = $graphObject['data'];
            
            // ALL NEXT PAGING, if exist
            $has_next = isset($paging->next) ? $paging->next : false;
            while ($has_next){
                $json = file_get_contents ($has_next);
                $json = json_decode($json);
                
                //save
                $all_fb_photos[] = $json->data;
                
                //next
                $has_next = isset($json->paging->next) ? $json->paging->next : false;
            }
            
            //prepare save path
            $save_path = "uploads/default/fb_photos/original/" . $this->current_user->id . "/";
            $thumb_path = "uploads/default/fb_photos/thumb/" . $this->current_user->id . "/";
            check_dir($save_path, '/');
            check_dir($thumb_path, '/');
            
            $saved_number=0;
            foreach($all_fb_photos as $page)
                foreach($page as $img){
                    /* for every fb photos */
                    
                    //thumbnail index
                    $i = count($img->images) -1;
                    //photo save path
                    $name = $img->id . ".jpg";
                    $img_path = FCPATH . $save_path . $name;
                    $thm_path = FCPATH . $thumb_path . $name;
                    
                    //fetch image
                    $image = file_get_contents($img->images[0]->source);
                    $thumb = file_get_contents($img->images[$i]->source);
                    
                    //save on local
                    file_put_contents($img_path, $image);
                    file_put_contents($thm_path, $thumb);
                    
                    //set photo info
                    $photo_info = array(
                        'tags'   => $img->tags,
                        'width'  => $img->images[0]->width,
                        'height' => $img->images[0]->height,
                        'source' => $img->images[0]->source
                    );
                    
                    //check whether this photo (with id) already exist on database
                    $params['stream'] = 'sn_photo';
                    $params['namespace'] = 'aisl';
                    $params['where'] = "photo_owner={$this->current_user->id}&&photo_sn='fb'&&photo_id='{$img->id}'";
                    $data = $this->streams->entries->get_entries($params);
                    //if not exist, insert
                    if ($data['total'] == 0){                    
                        //add to database
                        $save_data = array(
                            'photo_owner'=>$this->current_user->id, 'photo_sn' => 'fb', 'photo_id' => $img->id,
                            'photo_path'=>$save_path, 'photo_info' => json_encode($photo_info)
                        );
                        $this->streams->entries->insert_entry($save_data, 'sn_photo', 'aisl');
                        $saved_number++;
                    }
                }
            
            //finally redirect to images
            $this->session->set_flashdata('Fetching ' . $saved_number . ' photos is a success!'); 
            
            redirect('student/face');
            
        }
	}
	
	/**
	*  Download all foto on facebook smootly, in the sense of one by one:
	*  1. Using the user_id of account, fetch the first photo in Facebook
	*  2. Download and save this photo, display progress on page
	*  3. Fetch next url of the photo, and do the same until next image inexistent
	*  With this procedure, we can display how many images have been downloaded.
	*  However, we cannot show progress bar as we do not know total number of photo exists in FB.
	*  This function will use ddl() function to process each of next image.
	*  This function also connect with ServerFace to do face detection of the image.
	*
	*  TODO: First image is not consistently saved/displayed as next images.
	*/
	public function download(){
	
		$fb_token = $this->session->userdata('fb_token');
		$fb_uid = $this->session->userdata('fb_uid');
		if (isset($fb_token) && $fb_token != ""){
			$params = array('fields'=>'id');
			$graphObject = $this->get_photos($fb_token, $params);
			//$graphObject = json_decode(json_encode($graphObject), FALSE);
			
			$data["photos"] = $graphObject['data'];			
			$data["paging"] = $graphObject['paging'];
			
			//var_dump($data);
			
			
			// Show download Page
			$this->template->title(lang('face:fb_dl'));
			$this->template->build('fb_smooth_download', $data);
		}else{
            redirect('student/fb/login');
		}
	}
	
	/** 
	 * Fetch all id of Facebook photos and send to client. Client will do download one by one themself.
	 * This method send request facebook as long as there is "after" on the request.
	 * Effectively accoumulating all photos that are available in the Facebook.	 
	 */
	public function downbatch(){
		$fb_token = $this->session->userdata('fb_token');
		$fb_uid = $this->session->userdata('fb_uid');
		if (isset($fb_token) && $fb_token != ""){
			$params = array('fields'=>'id');
			if (isset($_GET["after"])) $params['after'] = $_GET["after"];
			$data = array('photos'=>array());
			
			$hasNextPage = true;
			$counter = 0;
			while ($hasNextPage && $counter < 10){
				$graphObject = $this->get_photos($fb_token, $params);
				$photos = isset($graphObject['data']) ? $graphObject['data'] : null;			
				$paging = isset($graphObject['paging']) ? $graphObject['paging'] : null;
				if (isset($paging->cursors->after)){
					$params['after'] = $paging->cursors->after;
					$data["photos"] = array_merge($data["photos"], $photos);
				}else{
					// stop
					$hasNextPage = false;
				}
				$counter++;
			}
			
			echo json_encode($data);
		}else{
			echo json_encode(array());
		}
	}

	/**
	  *  Process an image of Facebook using "after" parameter.
	  *  This means it will provide next image of certain image.
	  *  The processing involves fetching and saving image content, tags, and thumbnails
	  *  then send json to page (called in download function) to be displayed.
	  @OBSOLETE
	  */
	public function ddl(){
		$fb_token = $this->session->userdata('fb_token');
		$fb_uid = $this->session->userdata('fb_uid');
		$this->load->model('face_m');
		if (isset($fb_token) && $fb_token != ""){
			
			// Build parameter for fetching image from facebook
			$params = array(
				'access_token' => $fb_token,
				'limit' => 1,
			);
			// Get parameter from browser (for image after initial)
			if (isset($_GET["after"])) $params['after'] = $_GET["after"];
			// Fetch json from graph api
			$url = "https://graph.facebook.com/v2.2/{$fb_uid}/photos?" . http_build_query($params);
			$json = file_get_contents ($url);
			
			file_put_contents("log.txt", "ddl url=" . $url . "\r\n", FILE_APPEND);
			file_put_contents("log.txt", "ddl json=" . $json . "\r\n", FILE_APPEND);
			
			$json = json_decode($json);
			
			file_put_contents("log.txt", "ddl ndata=" . count($json->data) . "\r\n", FILE_APPEND);
			$json_data = $json->data;
			if (count($json_data)>0){
				$img = $json_data[0];
				$img_tag = null;
				$img_tag_x = -1;
				$img_tag_y = -1;
				
				foreach($img->tags->data as $tag){
					if (isset($tag->id) and $tag->id == $fb_uid){
						$img_tag = $tag;
						break;
					}
				}
				
				if ($img_tag !== null){
					$img->my_tag =  (object) array(
						"x" => $img_tag->x,
						"y" => $img_tag->y
					);
					$img_tag_x = $img_tag->x;
					$img_tag_y = $img_tag->y;
				}
				
				// Return string to browser for notification
				$return_json = array(
					'url' => $url,
					'paging' => $json->paging,
					'source' => 'web',
					'request' => 'detect',
					'info' => array(
						'slug' => 'fb',
						'user' => $img->from->id,
						'uid' => $this->current_user->id,
						'iid' => $img->id,
						'picture' => $img->picture,
						'source' => $img->images[0]->source,
						'path' => $this->face_m->fbsave_path,
						'x' => $img_tag_x,
						'y' => $img_tag_y,
					)
				);
				echo json_encode($return_json);
				
				// === Save this image into database (if image present) 
				// Save image
				if (isset($img)){
					//check $img Validity
					$this->face_m->saveFace($img);
				}
			}else{
				// Data is empty, download finished?
				$return_json = array('url' => 'finished');
				echo json_encode($return_json);
			}
		}
	}
	
	function downone($fb_photoid){
		
		$fb_token = $this->session->userdata('fb_token');
		
		if (isset($fb_token) && $fb_token != ""){
            $session = new FacebookSession($fb_token);
			$request = new FacebookRequest(
			  $session,
			  'GET',
			  '/'.$fb_photoid
			);

			$response = $request->execute();
			$graphObject = $response->getGraphObject();
			$graphObject = $graphObject->asArray();
			$graphObject = json_decode(json_encode($graphObject), FALSE);
			/* save face image, return to client is handled in below function*/		
			$this->save_fbimg($graphObject);
		}
	}
	
	function save_fbimg($img){
		$fb_uid = $this->session->userdata('fb_uid');
		$this->load->model('face_m');
		
		$img_tag = null;
		$img_tag_x = -1;
		$img_tag_y = -1;
		
		foreach($img->tags->data as $tag){
			if (isset($tag->id) and $tag->id == $fb_uid){
				$img_tag = $tag;
				break;
			}
		}
		
		if ($img_tag !== null){
			$img->my_tag =  (object) array(
				"x" => $img_tag->x,
				"y" => $img_tag->y
			);
			$img_tag_x = $img_tag->x;
			$img_tag_y = $img_tag->y;
		}
		
		// Return string to browser for notification
		$return_json = array(
			//'url' => $url,
			//'paging' => $json->paging,
			'source' => 'web',
			'request' => 'detect',
			'info' => array(
				'slug' => 'fb',
				'from' => $img->from->id,
				'uid' => $this->current_user->id,
				'iid' => $img->id,
				'picture' => $img->picture,
				'source' => $img->images[0]->source,
				'path' => $this->face_m->fbsave_path,
				'x' => $img_tag_x,
				'y' => $img_tag_y,
			),
		);
		
		// === Save this image into database (if image present) 
		// Save image
		if (isset($img)){
			//check $img Validity
			$dbid = $this->face_m->saveFace($img);
			if ($dbid){
				$return_json['dbid'] = $dbid;
				echo json_encode($return_json);
			}
		}
	}
	
	
	function reqserver_fbimg(){
		$this->load->model('face_m');
		if (!empty($_POST)){
			$reqserv_info = $_POST["reqserv_info"];
			//var_dump(json_encode($reqserv_info));
			$this->face_m->requestServerForDetection(json_encode($reqserv_info));
		}
	}
	
}


