<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class Classes extends Public_Controller
{
    private $data = array();

    /**
     * The constructor
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->lang->load('face');
        $this->lang->load('buttons');
        $this->load->driver('Streams');
        //$this->template->append_css('module::faq.css');
    }
     /**
     * List all FAQs
     *
     * We are using the Streams API to grab
     * data from the faqs database. It handles
     * pagination as well.
     *
     * @access	public
     * @return	void
     */
    public function index()
    {	
		if (!$this->current_user) redirect('users/login');
	
        // Get stream entries
        $params = array();
        $params['stream'] = 'attendance';
        $params['namespace'] = 'aisl';
        $params['where'] = "student={$this->current_user->id}";
        $data = $this->streams->entries->get_entries($params);
		
		$this->template->title(lang('face:list'));
        $this->template->build('list_class', $data);
	}
	
	public function view($id){
		if (!$this->current_user) redirect('users/login');
		
		// Get class entry
		$data["class"] = $this->streams->entries->get_entry($id, 'class', 'aisl');
		
        // Get this attendance entries
        $params = array();
        $params['stream'] = 'attendance';
        $params['namespace'] = 'aisl';
        $params['where'] = "class={$id}&&student={$this->current_user->id}";
        $data["my_att"] = $this->streams->entries->get_entries($params);
        $data["my_att"] = $data["my_att"]["entries"][0];
		
        // Get all list attendance in this class
        $params = array();
        $params['stream'] = 'attendance';
        $params['namespace'] = 'aisl';
        $params['where'] = "class={$id}";
        $data["list_student"] = $this->streams->entries->get_entries($params);
		
		$this->template
			->append_css('module::jquery-ui/jquery-ui.css')
			->append_js('module::jquery-ui/jquery-ui.js');
		$this->template->title(lang('class:view'));
        $this->template->build('view_class', $data);
	}
	
	public function update_cmt(){
	
	    $id = $_POST['id'];
	    $class_id = $_POST['class_id'];
		$comment = $_POST['comment'];
	
		$entry_data = array(
			'comment'    => $comment
		);
		$this->streams->entries->update_entry($id, $entry_data, 'attendance', 'aisl');
		
        redirect('student/class/view/'.$class_id);
	}
	
}
