<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class Info extends Public_Controller
{
    private $data = array();
    
	public function __construct()
    {
        parent::__construct();
        $this->load->helper('my_file');
	}

	/* Get JSON of Students ID */
	public function get_json($class = 0, $student = 0){
	
		$password = $_GET['pwd'];
		
		if ($class == 0){
			// Get user info
			$this->load->model(array('users/user_m'));
			$user = $this->user_m->get(array('id' => $student));
			
			$info = array(
				"id" => $student,
				"last_name" => $user->last_name,
				"first_name" => $user->first_name,
				"name" => $user->display_name,
				"affiliation" => $user->company,
				"class" => array()
			);
		}else{
			// Get student and class info
			$params = array();
			$params['stream'] = 'attendance';
			$params['namespace'] = 'aisl';
			$params['where'] = "student={$student}&&class={$class}";
			$class_info = $this->streams->entries->get_entries($params);
			
			if ($class_info['total']!=0) {
				$class_info = $class_info['entries'][0];
				
				$last_name  = $class_info['student']['last_name'];
				$first_name = $class_info['student']['first_name'];
				$display_name = $first_name . " " . $last_name;
				$class_name = $class_info['class']['name'];
				$class_code = $class_info['class']['code'];
				$attendance = $class_info['attendance'];
				$comment = $class_info['comment'];
				
				//TODO: fetch user photo
				$fb_face_url = "uploads/default/fb_photos/face/" .  $student . "/";
				$fu_face_url = "uploads/default/fu_photos/face/" .  $student . "/";
				$fb_face_path = FCPATH . $fb_face_url;
				$fu_face_path = FCPATH . $fu_face_url;
				
				$possible_path = array();
				$possible_count= 0;
				// extract from uploaded face
				if (is_dir($fu_face_path) && $handle = opendir($fu_face_path)) {
					while (($file = readdir($handle)) !== false){
						if (!in_array($file, array('.', '..')) && !is_dir($fu_face_path.$file)) {
							$possible_path[] = $fu_face_url.$file;
							$possible_count++;
						}
					}
				}
				// extract from facebook face
				if ($possible_count==0 && is_dir($fb_face_path) && $handle = opendir($fb_face_path)) {
					while (($file = readdir($handle)) !== false){
						if (!in_array($file, array('.', '..')) && !is_dir($fb_face_path.$file)){
							$possible_path[] = $fb_face_url.$file;
							$possible_count++;
						}
					}
				}
				
				// if at least one face picture is present
				$selection_path = "";
				if ($possible_count != 0){
					//randomize selection
					$selection_idx = rand(0, $possible_count - 1);
					$selection_path = $possible_path[$selection_idx];
					//echo base_url() . $selection_path;
				}
		
				$info = array(
					"id" => $student,
					"last_name" => $last_name,
					"first_name" => $first_name,
					"display_name" => $display_name,
					"class_name" => $class_name,
					"class_code" => $class_code,
					"attendance" => $attendance,
					"comment" => $comment,
					"picture" => $selection_path
				);
				
			}else{
				$info = array();
			}
		}
		
		// Return JSON to requester
		$json = array(
			"source" => "web",
			"request" => "info",
			"info" => $info
		);
		
		echo json_encode($json);
	}
	
	/* Get JSON of Students ID */
	public function img_pop($slug, $id = ""){
		$path = "uploads/default/";
		if ($slug == "fb"){
			/* Handle image pop up request from Facebook Images */
			$ori = $this->streams->entries->get_entry($id, 'sn_photo', 'aisl');
			// Compose data for view file
			$data['img_id'] = $id;
			$data['img_owner'] = $ori->photo_owner;
			$data['img_path'] = base_url() . $ori->photo_path . 'original/' .  $data['img_owner'] . '/' . $ori->photo_id . '.jpg';
			$data['img_info'] = json_decode(utf8_kill_entity_decode($ori->photo_info));
			// Set face area candidates properties
			if (property_exists($data['img_info'], "candidates")){
				$data['face_cds'] = (object) array("candidates" => $data['img_info']->candidates, "selection" => $data['img_info']->selection);
			}else{
				$data['face_cds'] = (object) array("candidates" => array(), "selection" => -1);
			}
		}else if ($slug == 'file'){
			/* Handle image pop up request from Uploaded Images */
			$img = $this->streams->entries->get_entry($id, 'face', 'aisl');
			// Compose data for view file
			$data['img_id'] = $id;
			$data['img_owner'] = $img->owner;
			$data['img_path'] = $img->face['image'];
			$data['img_info'] = (object) array(
				'width' => $img->face['width'], 
				'height'=> $img->face['height']
			);
			// Set face area candidates properties
			$data['face_cds'] = json_decode(utf8_kill_entity_decode($img->face_info));
		}
		
		$this->template->set_layout(false);
        $this->template->build('img_pop', $data);
	}
	
	/*
	
	*/
	public function teacher_login(){
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		
		// aunthetication
		if ($this->ion_auth->login($username, $password)){
			$user = $this->ion_auth->get_user_by_username($username);
			$user_id = $user->id;
			
			// proceed if the user is a teacher
			if ($user->group == "teacher"){
				
				// write a success json respon
				$response = array(
					"tag" => "login",
					"success" => 1,
					"error" => 0,
					"uid" => $user_id,
					"user" => array(
						"display_name" => $user->display_name,
					),
					"class" => array()
				);
				
				// find the classes assigned to this teacher
				$params = array();
				$params['stream'] = 'teacher';
				$params['namespace'] = 'aisl';
				$params['order_by'] = 'class_ref';
				$params['sort'] = 'asc';
				$params['where'] = "teacher_ref={$user_id}";
				$list_class = $this->streams->entries->get_entries($params);
				$trimmed_list_class;
				
				foreach($list_class["entries"] as $cl){
					$a_class["id"]   = $cl["class_ref"]["id"];
					$a_class["code"] = $cl["class_ref"]["code"];
					$a_class["name"] = $cl["class_ref"]["name"];
					$a_class["room"] = $cl["class_ref"]["room"];
					
					$trimmed_list_class[] = $a_class;
				}
				
				$response['class'] = $trimmed_list_class;
				
				// Build the response as json
				$json_response = json_encode($response);
				echo $json_response;
			}else{
				// write a success json respon
				$response = array(
					"tag" => "login",
					"success" => 0,
					"error" => 1,
					"message"=> "Your account is not a teacher group.",
					"user" => array(),
					"class" => array()
				);
				// Build the response as json
				$json_response = json_encode($response);
				echo $json_response;
			}
		}else{
			// write a success json respon
			$response = array(
				"tag" => "login",
				"success" => 0,
				"error" => 1,
				"message"=> "Wrong username/password.",
				"user" => array(),
				"class" => array()
			);
			// Build the response as json
			$json_response = json_encode($response);
			echo $json_response;
		}
	}

}