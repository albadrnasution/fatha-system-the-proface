<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 *
 * 
 * 
 * 
 * 
 *
 * 
 * 
 *
 * @author 		Albadr
 * @package 	AISL Student Face Manager
 */
class Admin_class extends Admin_Controller
{
    // This will set the active section tab
   protected $section = 'class';

    public function __construct()
    {
        parent::__construct();

        $this->lang->load('face');
        $this->load->driver('Streams');
    }
	
	/**
     * List all face images.
     *
     * @return	void
     */
	public function index()
    {
		redirect('student/admin/class/all');
    }
	
	public function all()
    {
	    // Get our entries. We are simply specifying the stream/namespace, 
		// and then setting the pagination up.
        $params = array(
            'stream' => 'class',
            'namespace' => 'aisl',
            'paginate' => 'yes',
            'limit' => 25,
            'pag_segment' => 5,
			'order_by' => 'code',
			'sort' => 'asc',
        );
        $data['classes'] = $this->streams->entries->get_entries($params);

        // Build the page. See views/admin/index.php
        // for the view code.
        $this->template
				->title(lang('class:list'))
				->build('admin/all_class', $data);  
    }
	
	public function create()
    {
        $extra = array(
            'return' => 'student/admin/class',
            'success_message' => lang('faq:submit_success'),
            'failure_message' => lang('faq:submit_failure'),
            'title' => 'New Class',
         );

        $this->streams->cp->entry_form('class', 'aisl', 'new', null, true, $extra);
    }

    public function edit($id = 0)
    {
        $extra = array(
            'return' => 'student/admin/class',
            'success_message' => lang('faq:submit_success'),
            'failure_message' => lang('faq:submit_failure'),
            'title' => 'Edit Class',
         );

        $this->streams->cp->entry_form('class', 'aisl', 'edit', $id, true, $extra);
    }
	
	public function delete($id = 0)
    {
        $this->streams->entries->delete_entry($id, 'class', 'aisl');
        $this->session->set_flashdata('error', lang('Error!'));
 
        redirect('student/admin/class');
    }
	
    public function view($id = 0)
    {	
		//if user that access this function has role teacher
		//check whether he is assigned to this class or not
		//refuse access if he is not assigned
		
		if ($this->current_user->group_id == 3){
			//check whether current user has assigned as teacher on class $cls
			$params = array();
			$params['stream'] = 'teacher';
			$params['namespace'] = 'aisl';
			$params['where'] = "class_ref={$id}&&teacher_ref={$this->current_user->id}";
			$list_teacher = $this->streams->entries->get_entries($params);
			if ($list_teacher['total'] == 0) {
				//refuse access
				echo "You are not assigned to this class.";
				die;
			}
		}
	
	
        // Get class entries
        $data['class'] = $this->streams->entries->get_entry($id, 'class', 'aisl', false);
        // Get student entries
        $params = array();
        $params['stream'] = 'attendance';
        $params['namespace'] = 'aisl';
        $params['order_by'] = 'student';
        $params['sort'] = 'asc';
        $params['where'] = "class={$id}";
        $data['list_student'] = $this->streams->entries->get_entries($params);
        // Get teacher entries
        $params = array();
        $params['stream'] = 'teacher';
        $params['namespace'] = 'aisl';
        $params['order_by'] = 'teacher_ref';
        $params['sort'] = 'asc';
        $params['where'] = "class_ref={$id}";
        $data['list_teacher'] = $this->streams->entries->get_entries($params);
		
		// Generating student and teacher list for exclusion to new student/teacher
		$std_ids = array(); $tch_ids = array();
		foreach ($data['list_student']['entries'] as $ls){
			$std_ids[] = $ls['student']['id'];
		}
		foreach ($data['list_teacher']['entries'] as $ls){
			$tch_ids[] = $ls['teacher_ref']['id'];
		}
		
		// Loading available teacher and student
		$this->load->model(array('users/user_m'));
		$teacher_pool = $this->user_m->get_many_by(array('group_id' => 3));
		$student_pool = $this->user_m->get_many_by(array('group_id' => 2));
		
		// Exclude existing student/teacher from the list of new student/teacher
		$data['user_teacher'] = array();
		$data['user_student'] = array();		
		foreach ($teacher_pool as $ls){
			if (!in_array($ls->id, $tch_ids)){
				$data['user_teacher'][] = $ls;
			}
		}		
		foreach ($student_pool as $ls){
			if (!in_array($ls->id, $std_ids)){
				$data['user_student'][] = $ls;
			}
		}
		
        // Preparation for adding student
        $this->load->library('streams_core/Fields');

        $stream = $this->streams->streams->get_stream('attendance', 'aisl');
        $namespace = 'aisl';
        $mode = 'new';
        $entry = null;
        $plugin = false;
        $recaptcha = false;
        $extra = array(
            'return' => 'admin/student/class/view/'.$id,
            'success_message' => lang('event:events:submit_success'),
            'failure_message' => lang('event:events:submit_failure'),
            'title' => 'lang:event:events:new',
        );
        $skips = array();
        $defaults = array();

        // Get our field form elements.
        $data['fields'] = $this->fields->build_form($stream, $mode, $entry, $plugin, $recaptcha, $skips, $extra, $defaults);
        $data['stream'] = $stream;
        $data['mode'] = $mode;
        $data['return'] = $extra['return'];
        $data['entry'] = $entry;

        // Make input_slug as the array key
        // in order to ease form templating.
        if (is_array($data['fields'])) {
            $fields_arr = array();
            foreach ($data['fields'] as $field) {
                $fields_arr[$field['input_slug']] = $field;
            }
            $data['fields'] = $fields_arr;
		}
		
		
		$this->template
			->append_css('module::jquery-ui/jquery-ui.css')
			->append_js('module::jquery-ui/jquery-ui.js');
		$this->template->title(lang('class:list_student'));
        $this->template->build('admin/view_class', $data);
    }
	
	public function train_fm($id = 0){
        $this->config->load('config');
		// Get class entries
        $class = $this->streams->entries->get_entry($id, 'class', 'aisl', false);
        // Get student entries
        $params = array();
        $params['stream'] = 'attendance';
        $params['namespace'] = 'aisl';
        $params['where'] = "class={$id}";
        $students = $this->streams->entries->get_entries($params);
		
		$list_student = array();
		foreach($students['entries'] as $std){
			$list_student[] = $std["student"]["id"];
		}
		
		$data['json'] = (object) array(
			'source' => 'web',
			'request' => 'train',
			'info' => array (
				"class_id" => $class->id,
				"class_name" => $class->name,
				"class_code" => $class->code,
				"student_ids" => $list_student,
			)
		);
		$data['class'] = $class;

		$this->template->title('');
		$this->template->build('admin/command_train', $data);
		
	}
	
	public function update_info($id = 0){
		//process only if id non-zero
		if ($id > 0){
			// Convert received info from json to object
			$training_info = json_decode($_POST["training_info"]);
			// Get class entries
			$class = $this->streams->entries->get_entry($id, 'class', 'aisl', false);
			// Add training info
			$class_info_obj = json_decode($class->class_info);
			if (empty($class_info_obj)) $class_info_obj = (object) array();
			if (!property_exists($class_info_obj, 'training')) 
				$class_info_obj->training = array();
			$class_info_obj->training[] = $training_info;
			
			// Compose update item
			$entry_data = array(
				'class_info' => json_encode($class_info_obj)
			);
			// Update class table
			$return = $this->streams->entries->update_entry($id, $entry_data,'class', 'aisl');
			// Return something
			if ($return == $id)
				echo "updated";
			else
				echo "failed";
		}else{
			echo "Wrong ID.";
		}
	}
	
	public function assign_teacher(){
		if ($_POST["teacher_id"] && $_POST["class_id"]){
			$entry_data = array(
				"class_ref" => $_POST["class_id"],
				"teacher_ref" => $_POST["teacher_id"],
			);
			$this->streams->entries->insert_entry($entry_data, "teacher", "aisl");
			redirect('student/admin/class/view/' . $_POST["class_id"]);
		}
	}

	public function assign_student(){
		if ($_POST["student_id"] && $_POST["class_id"]){
			$entry_data = array(
				"class" => $_POST["class_id"],
				"student" => $_POST["student_id"],
			);
			$this->streams->entries->insert_entry($entry_data, "attendance", "aisl");
			redirect('student/admin/class/view/' . $_POST["class_id"]);
		}
	}
    
}

