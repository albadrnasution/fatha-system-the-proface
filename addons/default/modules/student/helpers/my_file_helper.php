<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('file_force_contents'))
{
    function file_force_contents($dir, $contents){
        $parts = explode('/', $dir);
        $file = array_pop($parts);
        $dir = '';
        foreach($parts as $part)
            if(!is_dir($dir .= "$part/")) mkdir($dir);
        file_put_contents("$dir/$file", $contents);
    }
}

if ( ! function_exists('check_dir'))
{
	/* make sure this dir path is exist on every level, return the filename */
    function check_dir($dir, $dir_char='/'){
        $parts = explode($dir_char, $dir);
        $file = array_pop($parts);
        $dir = '';
        foreach($parts as $part)
            if(!is_dir($dir .= "$part/")) mkdir($dir);
		return $file;
    }
}


if ( ! function_exists('utf8_entity_decode'))
{
	function utf8_entity_decode($entity){
		$convmap = array(0x0, 0x10000, 0, 0xfffff);
		return mb_decode_numericentity($entity, $convmap, 'UTF-8');
	}
}

if ( ! function_exists('utf8_kill_entity_decode'))
{
	function utf8_kill_entity_decode($string){
		return preg_replace_callback('/&#\d{2,5};/u', function($match){return utf8_entity_decode($match[0]);}, $string );
	}
}
