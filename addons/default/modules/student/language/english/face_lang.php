<?php
//messages
$lang['face:success']		=	'It worked';
$lang['face:error']			=	'It didn\'t work';
$lang['face:no_items']		=	'No Items';

//creation
$lang['face:create']		=	'Create New';
$lang['face:add']			=	'Add';

//labels
$lang['face:name']			=	'Name';
$lang['face:slug']			=	'Slug';
$lang['face:manage']		=	'Manage';
$lang['face:list']			=	'Face List';
$lang['face:fb_dl']			=	'Download Face from Facebook';
$lang['face:view']			=	'View';
$lang['face:edit']			=	'Edit';
$lang['face:delete']		=	'Delete';

//buttons
$lang['face:custom_button']	=	'Custom Button';
$lang['face:items']			=	'Items';


$lang['face:face']			=	'Faces';

//class label
$lang['class:class']		=	'Classes';
$lang['class:list']			=	'Classes List';
$lang['class:list_student']	=	'Student in this Class';
$lang['class:no_student']	=	'No student.';
$lang['class:no_teacher']	=	'No teacher assigned to this class.';
$lang['class:name']			=	'Subject Name';
$lang['class:code']			=	'Subject Code';
$lang['class:room']			=	'Subject Room';
$lang['class:view']			=	'Class Detail';

//class 
$lang['attendance:attendance']	=	'Attendance';
$lang['attendance:list']		=	'Attendance List';


?>