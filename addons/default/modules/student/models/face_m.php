<?php defined('BASEPATH') or exit('No direct script access allowed');

require_once( FCPATH . 'vendor/autoload.php' );
/**
 * Models for saving and fetching face image from database.
 *
 * @author		Albadr Nasution
 */
class Face_m extends MY_Model {
	// fb folder path
	public $fbsave_path;
	public $fbdelete_path;
	public $fbsave_orign;
	public $fbsave_thumb;
	public $fbsave_face;
	public $fbsave_face2;
	public $fbsave_misc;
	// image database path
	public $orign_path;
	public $thumb_path;
	public $face_path;
	public $face_path2;
	public $misc_path;
	// images from file_uploaded
	public $fusave_path;
	public $fusave_img;

	public function __construct()
	{
		parent::__construct();
	
		$this->_table = 'sn_photo';
		$this->fbdelete_path = 'uploads/default/fb_deleted/';
		$this->fbsave_path = 'uploads/default/fb_photos/';
		$this->fbsave_orign = 'original/';
		$this->fbsave_thumb = 'thumb/';
		$this->fbsave_face = 'face/';
		$this->fbsave_face2 = 'face_exclude/';
		$this->fbsave_misc = 'misc/';
		
		$this->fusave_path = 'uploads/default/fu_photos/';
		
		//prepare save path
		$this->orign_path = $this->fbsave_path . $this->fbsave_orign . $this->current_user->id . "/";
		$this->thumb_path = $this->fbsave_path . $this->fbsave_thumb . $this->current_user->id . "/";
		$this->face_path = $this->fbsave_path . $this->fbsave_face . $this->current_user->id . "/";
		$this->face_path2 = $this->fbsave_path . $this->fbsave_face2 . $this->current_user->id . "/";
		$this->misc_path = $this->fbsave_path . $this->fbsave_misc . $this->current_user->id . "/";
		$this->fusave_img = $this->fusave_path . "candidate/" . $this->current_user->id . "/";
		//first check whether the dir is exist
		if (!is_dir($this->orign_path)) check_dir($this->orign_path, '/');
		if (!is_dir($this->thumb_path)) check_dir($this->thumb_path, '/');
		if (!is_dir($this->face_path)) check_dir($this->face_path, '/');
		if (!is_dir($this->face_path2)) check_dir($this->face_path, '/');
		if (!is_dir($this->misc_path)) check_dir($this->misc_path, '/');
		if (!is_dir($this->fusave_img)) check_dir($this->fusave_img, '/');
	}
	
	public function deleteFbFace($id){
		
        $fbi = $this->streams->entries->get_entry($id, 'sn_photo', 'aisl');
		if ($fbi){
			$photo_info = utf8_kill_entity_decode($fbi->photo_info);
			$photo_info_obj = json_decode(utf8_kill_entity_decode($fbi->photo_info));
			
			$fc_now_link = FCPATH . $this->fbsave_path   . "face/" . $fbi->photo_owner . "/" . $fbi->photo_id . ".jpg";
			if (property_exists($photo_info_obj, 'exclude') and $photo_info_obj->exclude=="yes")
				$fc_now_link = FCPATH . $this->fbsave_path   . "face_exclude/" . $fbi->photo_owner . "/" . $fbi->photo_id . ".jpg";
			
			$fc_del_link = FCPATH . $this->fbdelete_path . "face/" . $fbi->photo_owner . "-" . $fbi->photo_id . ".jpg";
			if (file_exists($fc_now_link)) rename($fc_now_link, $fc_del_link);
			
			$or_now_link = FCPATH . $this->fbsave_path   . "original/" . $fbi->photo_owner . "/" . $fbi->photo_id . ".jpg";
			$or_del_link = FCPATH . $this->fbdelete_path . "original/" . $fbi->photo_owner . "-" . $fbi->photo_id . ".jpg";
			if (file_exists($or_now_link)) rename($or_now_link, $or_del_link);
			
			$th_now_link = FCPATH . $this->fbsave_path   . "thumb/" . $fbi->photo_owner . "/" . $fbi->photo_id . ".jpg";
			$th_del_link = FCPATH . $this->fbdelete_path . "thumb/" . $fbi->photo_owner . "-" . $fbi->photo_id . ".jpg";
			if (file_exists($th_now_link)) rename($th_now_link, $th_del_link);
			
			$info_link = FCPATH . $this->fbdelete_path   . "info/" . $fbi->photo_owner . "-" . $fbi->photo_id . ".txt";
			$infofile = fopen($info_link, "w");
			fwrite($infofile, $photo_info);
			fclose($infofile);
			
			// delete Facebook photos from database entry
			$this->streams->entries->delete_entry($id, 'sn_photo', 'aisl');
		}
	}
	public function deleteFbAll(){
		$params = array(
			'stream'        => 'sn_photo',
			'namespace'     => 'aisl',
			'where'			=> "photo_owner={$this->current_user->id}&&photo_sn='fb'"
		);
        $fbimages = $this->streams->entries->get_entries($params);
		
		if ($fbimages["total"] > 0){
			// reiterate through to delete all
			$faceIds = array();
			foreach ($fbimages["entries"] as $fbi){
				$faceIds[] = $fbi['id'];
				//$this->deleteFbFace();
			}
			$faceIds_json = json_encode($faceIds);
			echo $faceIds_json;
		}
	}
	
	public function saveFace($img){
		
		// Set photo info
		$photo_info = array(
			'tags'   => $img->tags,
			'my_tag' => empty($img->my_tag) ? '' : $img->my_tag,
			'width'  => $img->images[0]->width,
			'height' => $img->images[0]->height,
			'source' => $img->images[0]->source,
			'exclude' => 'yes'
		);
		
		
		// Check whether this photo (with id) already exist on database
		$params['stream'] = 'sn_photo';
		$params['namespace'] = 'aisl';
		$params['where'] = "photo_owner={$this->current_user->id}&&photo_sn='fb'&&photo_id='{$img->id}'";
		$data = $this->streams->entries->get_entries($params);
		
		// Conditional of wheter image id exist or not
		$dbid = -1;
		if ($data['total'] == 0){                    
			//if image entry not exist, insert to database
			$save_data = array(
				'photo_owner'=>$this->current_user->id, 'photo_sn' => 'fb', 'photo_id' => $img->id,
				'photo_path'=>$this->fbsave_path , 'photo_info' => json_encode($photo_info),
				'date_taken'=>$img->created_time
			);
			$dbid = $this->streams->entries->insert_entry($save_data, 'sn_photo', 'aisl');

		}else{
			//if image entry exist in database, update info only
			$entry_in_db = $data['entries'][0];
			$entry_data = array(
				'photo_info' => json_encode($photo_info)
			);
			$dbid = $this->streams->entries->update_entry($entry_in_db['id'], $entry_data,'sn_photo', 'aisl');
		}
		
		// Save the file of image within server local directory
		try{
			//photo save path
			$name = $img->id . ".jpg";
			
			//thumbnail index
			$i = count($img->images) - 1;
			
			//bypass ssl for php 5.6 above
			$context = stream_context_create(array(
				'ssl' => array(
					"cafile" => FCPATH . "vendor/facebook/php-sdk-v4/src/Facebook/HttpClients/fb_ca_chain_bundle.crt",
					"verify_peer"=> true,
					"verify_peer_name"=> true,
				)
			));
			//fetch image
			$image = file_get_contents($img->images[0]->source, 0, $context);
			$thumb = file_get_contents($img->images[$i]->source, 0, $context);
			
			//save on local
			file_put_contents(FCPATH . $this->orign_path . $name, $image);
			file_put_contents(FCPATH . $this->thumb_path . $name, $thumb);
			
			return $dbid;
		}catch(Exception $e){
			//exception if something wrong
			echo "Something wrong: " . $e->getMessage();
			return false;
		}
	}
	
	/** JSON return include at least four component.
	 * Property "slug" to determine which image this update belong
	 * Property "img_id" to determine the index
	 * Property "canddidates" and "selection" to update the image
	 */
	public function face_update($jsonReturn){
		$received_info = json_decode($jsonReturn);
		
		$slug = $received_info->slug;
		
		// First check whether it is fb or uploads
		if ($slug == "fb"){
			/* Facebook Images: table is sn_photo */
			// Get facebook id of image
			$img_fbid = $received_info->img_id;
			
			// Get face based on facebook image_id
			$params['stream'] = 'sn_photo';
			$params['namespace'] = 'aisl';
			$params['where'] = "photo_owner={$this->current_user->id}&&photo_sn='fb'&&photo_id='{$img_fbid}'";
			$data = $this->streams->entries->get_entries($params);
			
			if (!empty($data['entries'][0])){
				// Continue update if the facebook image_id exist within database
				$face = $data['entries'][0];
		
				// Get json from column face_info
				$face_info_fromdb = $face['photo_info'];
				$face_info_fromdb = json_decode(utf8_kill_entity_decode($face_info_fromdb));
				// Update to new face_candidate
				$face_info_fromdb->candidates = $received_info->face_candidates;
				$face_info_fromdb->selection = $received_info->face_selection;
				// Compose update item
				$entry_data = array(
					'photo_info' => json_encode($face_info_fromdb)
				);
				// Update the SN_Photo table
				$return = $this->streams->entries->update_entry($face['id'], $entry_data,'sn_photo', 'aisl');
				// Return something
				$this->logger->notice("Returned update for : ".$return);
				echo $return;
			}
		}else{
			$img_id = $received_info->img_id;
			/* Uploaded Images: table is `face` */
			// Compose face info
			$face_info = (object) array(
				'candidates' => $received_info->face_candidates,
				'selection'  => -1
			);
			// Compose update item
			$entry_data = array(
				'face_info' => json_encode($face_info)
			);
			// Update the Face table
			$return = $this->streams->entries->update_entry($img_id, $entry_data,'face', 'aisl');
			// Return something
			$this->logger->notice("Returned update for : ".$return);
			echo $return;
			
		}
	}

	
	private $loop, $client, $logger, $writer;
	
	public function requestServerForDetection($requestJson){
		$this->loop = \React\EventLoop\Factory::create();

		$this->logger = new \Zend\Log\Logger();
		$this->writer = new Zend\Log\Writer\Stream(FCPATH . "log.ws.txt");
		$this->logger->addWriter($this->writer);
		$this->client = new \Devristo\Phpws\Client\WebSocket("ws://bee00.aisl.cs.tut.ac.jp:9002/", $this->loop, $this->logger);

		$this->client->on("request", function($headers) {
			$this->logger->notice("Request object created!");
		});

		$this->client->on("handshake", function() {
			$this->logger->notice("Handshake received!");
		});

		$this->client->on("connect", function($headers='') use ($requestJson) {
			$this->logger->notice("Connected!");	
			// Send json info about this particular image
			$this->logger->notice("Sending:" . $requestJson);	
			$this->client->send($requestJson);
		});

		$this->client->on("message", function($message){
			$this->logger->notice("Got message: ".$message->getData());
			$this->face_update($message->getData());
			$this->client->close();
		});

		$this->client->open();
		$this->loop->run();
	}
	
}
