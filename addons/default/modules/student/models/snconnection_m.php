<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Models for saving and fetching face image from database.
 *
 * @author		Albadr Nasution
 */
class Snconnection_m extends MY_Model {

	public function __construct()
	{
        $this->load->helper('my_file');
		parent::__construct();
	
	}
	
	public function updateConnection($user_id, $sn_user_id, $access_token){
		$sn_conn = $this->findConnection($user_id, $sn_user_id);
		
		if ($sn_conn){
			// update existing connection
            $save_obj = array('access_token'=>$access_token, 'user_id'=>$sn_user_id);
            $save_data = array('sn_owner'=>$user_id, 'sn_slug' => 'fb', 'sn_info' => json_encode($save_obj));
            $this->streams->entries->update_entry($sn_conn['id'], $save_data, 'sn_conn', 'aisl');
		}else{
			// insert new entry
            $save_obj = array('access_token'=>$access_token, 'user_id'=>$sn_user_id);
            $save_data = array('sn_owner'=>$user_id, 'sn_slug' => 'fb', 'sn_info' => json_encode($save_obj));
            $this->streams->entries->insert_entry($save_data, 'sn_conn', 'aisl');
		}
		
	}
	
	public function findConnection($uid, $sn_uid){
		// 
		$params['stream'] = "sn_conn";
		$params['namespace'] = "aisl";
		$params['where'] = "sn_owner='{$uid}'&&sn_slug='fb'";
		$data = $this->streams->entries->get_entries($params);
		
		foreach ($data['entries'] as $entry){
			$sn_info = json_decode(utf8_kill_entity_decode($entry["sn_info"]));
			if ($sn_info->user_id == $sn_uid && $entry['sn_owner']['id']!=null)
				return $entry;
		}
		return null;
	}

	public function findConnection_byWebpageID($web_uid){
		// 
		$params['stream'] = "sn_conn";
		$params['namespace'] = "aisl";
		$params['where'] = "sn_owner='{$web_uid}'&&sn_slug='fb'";
		$data = $this->streams->entries->get_entries($params);
		
		if ($data['total'] > 0){
			$entry = $data['entries'][0];
			
			//objectify json os sn_info on entry
            $entry['sn_info'] = json_decode(utf8_kill_entity_decode($entry['sn_info']));
			return $entry;
		}
		return null;
	}
	
	public function findConnection_bySocialUserID($sn_uid){
		// 
		$params['stream'] = 'sn_conn';
		$params['namespace'] = 'aisl';
		$data = $this->streams->entries->get_entries($params);
		
		foreach ($data['entries'] as $entry){
			$sn_info = json_decode(utf8_kill_entity_decode($entry["sn_info"]));
			if ($sn_info->user_id == $sn_uid && $entry['sn_owner']['id']!=null)
				return $entry;
		}
		return null;
	}
	
	
	public function deleteConnection($web_uid){
		// 
		$params['stream'] = "sn_conn";
		$params['namespace'] = "aisl";
		$params['where'] = "sn_owner='{$web_uid}'&&sn_slug='fb'";
		$data = $this->streams->entries->get_entries($params);
		if ($data['total'] > 0){
			foreach ($data['entries'] as $entry){
				$this->streams->entries->delete_entry($entry['id'], 'sn_conn', 'aisl');
			}
		}
	}
	
	/*
	*  Receive facebook user id, check to Facebook whether user has authorize to the FB.App.
	*  If yes, then find account of the user in this webpage and login.
	*  If no, return false;
	*/
	public function loginUser_withFacebookID($facebook_uid){
		$sn = $this->findConnection_bySocialUserID($facebook_uid);
		if ($sn != null){
			$this->ion_auth->force_login($sn['sn_owner']['id']);
		}
		return $sn;
	}
}