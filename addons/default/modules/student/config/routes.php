<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
| 	www.your-site.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://www.codeigniter.com/user_guide/general/routing.html
*/

// front-
$route['student']			= 'face';
$route['student/faces']		= 'face';
$route['student/class'] 	= 'classes';
$route['student/class/(:any)'] 	= 'classes/$1';
$route['admin/student/face']		= 'admin/student';
//$route['student/admin/face(:any)'] = 'admin_face$1';
$route['student/admin/class'] = 'admin_class';
$route['student/admin/class(:any)'] = 'admin_class$1';
$route['student/admin/attendance'] = 'admin_attendance';
$route['student/admin/attendance(:any)'] = 'admin_attendance$1';


