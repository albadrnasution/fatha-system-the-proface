<?php defined('BASEPATH') or exit('No direct script access allowed');

class Module_Student extends Module {

	public $version = '1.0';
	
    public function info()
    {
        return array(
            'name' => array(
				'en' => 'Student Face Manager'
            ),
            'description' => array(
                'en' => 'Managing face images of student, social network connection, and their classes.'
            ),
            'frontend' => true,
            'backend' => true,
            'menu' => '',
            'sections' => array(
                'face' => array(
                    'name' => 'face:face',
                    'uri' => 'admin/student',
                    'shortcuts' => array(
                        'create' => array(
                            'name' => 'face:add',
                            'uri' => 'admin/student/add_face',
                            'class' => 'add'
                        ),
                    )
                ),
                'class' => array(
                    'name' => 'class:class',
                    'uri' => 'student/admin/class',
                    'shortcuts' => array(
                        'create' => array(
                            'name' => 'face:create',
                            'uri' => 'admin/student/class/create',
                            'class' => 'add'
                        )
                    )
                )
            )
        );
    }
	
	public function admin_menu(&$menu)
	{
		$menu['Student Face Manager'] = "admin/student";
	}

	public function install()
	{
        $this->load->driver('Streams');
		
        $this->load->language('student/face');

        if (!$this->streams->streams->add_stream('Face Manager', 'face', 'aisl', 'aisl_', null)) return false;
		if (!$classes_stream_id = $this->streams->streams->add_stream('Class Manager', 'class', 'aisl', 'aisl_', null)) return false;
		if (!$this->streams->streams->add_stream('Attendance Manager', 'attendance', 'aisl', 'aisl_', null)) return false;
		if (!$this->streams->streams->add_stream('SN Connection', 'sn_conn', 'aisl', 'aisl_', null)) return false;
		if (!$this->streams->streams->add_stream('SN Photos', 'sn_photo', 'aisl', 'aisl_', null)) return false;
		if (!$this->streams->streams->add_stream('Class-Teacher', 'teacher', 'aisl', 'aisl_', null)) return false;
		if (!$users_stream = $this->streams->streams->get_stream('profiles', 'users'))
			return false;
		$users_stream_id = $users_stream->id;
			
		$field = array(
			array(
				'name' => 'Face owner',
				'slug' => 'owner',
				'type' => 'relationship',
				'extra' => array(
					'choose_stream' => $users_stream_id,
					//'link_uri' => '',
				),
				'title_column' => false,
				'required' => true,
				'unique' => false,
				'assign' => 'face',
				'namespace' => 'aisl',
			),
			array(
                'name' => 'Face Image',
				'slug' => 'face',
				'type' => 'image',
				'extra' => array(
					'folder' => 1,
					//'resize_width' => '',
					//'resize_height' => '',
					//'keep_ratio' => '',
					//'allowed_types' => '',
				),
				'title_column' => false,
				'required' => false,
				'unique' => false,
				'assign' => 'face',
                'namespace' => 'aisl',
			),
			array(
				'name' => 'Face Object Info',
				'slug' => 'face_info',
				'assign' => 'face',
				'namespace' => 'aisl',
				'type' => 'textarea',
				'required' => false,
				'unique' => false
			),
            /* ************************************************
             * CLASS's field
             * ************************************************ */
			array(
				'name' => 'Subject Code',
				'slug' => 'code',
				'type' => 'text',
				'extra' => array(
				//'max_length' => '',
				//'default_value' => '',
				),
				'required' => true,
				'unique' => true,
				'assign' => 'class',
				'namespace' => 'aisl',
			),
			array(
				'name' => 'Subject Name',
				'slug' => 'name',
				'type' => 'text',
				'extra' => array(
				//'max_length' => '',
				//'default_value' => '',
				),
				'title_column' => true,
				'required' => false,
				'unique' => false,
				'assign' => 'class',
				'namespace' => 'aisl',
			),
			array(
				'name' => 'Subject Room',
				'slug' => 'room',
				'type' => 'text',
				'extra' => array(
				//'max_length' => '',
				//'default_value' => '',
				),
				'required' => false,
				'unique' => false,
				'assign' => 'class',
				'namespace' => 'aisl',
			),	
			array(
				'name' => 'Subject Info',
				'slug' => 'class_info',
				'assign' => 'class',
				'namespace' => 'aisl',
				'type' => 'textarea',
				'required' => false,
				'unique' => false
			),
			
            /* ************************************************
             * ATTENDANCE's field
             * ************************************************ */
			array(
				'name' => 'Subject Class',
				'slug' => 'class',
				'type' => 'relationship',
				'extra' => array(
					'choose_stream' => $classes_stream_id,
					//'link_uri' => '',
				),
				'title_column' => true,
				'required' => true,
				'unique' => false,
				'assign' => 'attendance',
				'namespace' => 'aisl',
			),
			array(
				'name' => 'Student',
				'slug' => 'student',
				'type' => 'relationship',
				'extra' => array(
					'choose_stream' => $users_stream_id,
					//'link_uri' => '',
				),
				'title_column' => true,
				'required' => true,
				'unique' => false,
				'assign' => 'attendance',
				'namespace' => 'aisl',
			),
			array(
				'name' => 'Attendance',
				'slug' => 'attendance',
				'type' => 'integer',
				'extra' => array(
					//'max_length' => '',
					'default_value' => '100',
				),
				'title_column' => true,
				'required' => false,
				'unique' => false,
				'assign' => 'attendance',
				'namespace' => 'aisl',
			),
			array(
				'name' => 'Comment',
				'slug' => 'comment',
				'type' => 'text',
				'extra' => array(
					//'max_length' => '',
					//'default_value' => '',
				),
				'title_column' => true,
				'required' => false,
				'unique' => false,
				'assign' => 'attendance',
				'namespace' => 'aisl',
			),
			
			
            /* ************************************************
             * SOCIAL NETWORK's field
             * ************************************************ */
			array(
				'name' => 'Connection Owner',
				'slug' => 'sn_owner',
				'type' => 'relationship',
				'extra' => array(
					'choose_stream' => $users_stream_id,
					//'link_uri' => '',
				),
				'title_column' => false,
				'required' => true,
				'unique' => true,
				'assign' => 'sn_conn',
				'namespace' => 'aisl',
			),
			array(
				'name' => 'Social Network',
				'slug' => 'sn_slug',
				'type' => 'text',
				'extra' => array(
					'max_length' => '10',
					//'default_value' => '',
				),
				'title_column' => true,
				'required' => true,
				'unique' => true,
				'assign' => 'sn_conn',
				'namespace' => 'aisl',
			),
			array(
				'name' => 'SN Info Object',
				'slug' => 'sn_info',
				'assign' => 'sn_conn',
				'namespace' => 'aisl',
				'type' => 'textarea',
				'required' => false,
				'unique' => false
			),
            /* ************************************************
             * SN PHOTO's field
             * ************************************************ */
			array(
				'name' => 'Photo Owner',
				'slug' => 'photo_owner',
				'type' => 'relationship',
				'extra' => array(
					'choose_stream' => $users_stream_id,
					//'link_uri' => '',
				),
				'title_column' => false,
				'required' => true,
				'unique' => true,
				'assign' => 'sn_photo',
				'namespace' => 'aisl',
			),
			array(
				'name' => 'Social Network',
				'slug' => 'photo_sn',
				'type' => 'text',
				'extra' => array(
					'max_length' => '10',
					//'default_value' => '',
				),
				'title_column' => true,
				'required' => true,
				'unique' => true,
				'assign' => 'sn_photo',
				'namespace' => 'aisl',
			),
			array(
				'name' => 'Identifier',
				'slug' => 'photo_id',
				'type' => 'text',
				'extra' => array(),
				'title_column' => true,
				'required' => true,
				'unique' => true,
				'assign' => 'sn_photo',
				'namespace' => 'aisl',
			),
			array(
				'name' => 'Path',
				'slug' => 'photo_path',
				'type' => 'text',
				'extra' => array(),
				'title_column' => false,
				'required' => false,
				'unique' => false,
				'assign' => 'sn_photo',
				'namespace' => 'aisl',
			),
			array(
				'name' => 'Date Taken',
				'slug' => 'date_taken',
				'type' => 'text',
				'extra' => array(),
				'title_column' => false,
				'required' => false,
				'unique' => false,
				'assign' => 'sn_photo',
				'namespace' => 'aisl',
			),
			array(
				'name' => 'SN Photo Object',
				'slug' => 'photo_info',
				'assign' => 'sn_photo',
				'namespace' => 'aisl',
				'type' => 'textarea',
				'required' => false,
				'unique' => false
			),
            /* ************************************************
             * TEACHER CLASS RELATION
             * ************************************************ */
			array(
				'name' => 'Class',
				'slug' => 'class_ref',
				'type' => 'relationship',
				'extra' => array(
					'choose_stream' => $classes_stream_id,
					//'link_uri' => '',
				),
				'title_column' => false,
				'required' => true,
				'unique' => true,
				'assign' => 'teacher',
				'namespace' => 'aisl',
			),
			array(
				'name' => 'Teacher',
				'slug' => 'teacher_ref',
				'type' => 'relationship',
				'extra' => array(
					'choose_stream' => $users_stream_id,
					//'link_uri' => '',
				),
				'title_column' => false,
				'required' => true,
				'unique' => true,

				'assign' => 'teacher',
				'namespace' => 'aisl',
			),
		);

		
        $this->streams->fields->add_fields($field);
		return TRUE;
	}

	public function uninstall()
	{
        $this->load->driver('Streams');

        // For this teardown we are using the simple remove_namespace
        // utility in the Streams API Utilties driver.
        $this->streams->utilities->remove_namespace('aisl');

        return true;
	}


	public function upgrade($old_version)
	{
		// Your Upgrade Logic
		if ($old_version=="1.1"){
			$version="1.0";
			
			$this->load->driver('Streams');
			$this->load->language('student/face');
			
			if (!$users_stream = $this->streams->streams->get_stream('profiles', 'users'))
				return false;
			if (!$class_stream = $this->streams->streams->get_stream('class', 'aisl'))
				return false;
			
			$field = array(	);			
			
			$this->streams->fields->add_fields($field);
		}
	
		return TRUE;
	}

	public function help()
	{
		// Return a string containing help info
		// You could include a file and return it here.
		return "No documentation has been added for this module.<br />Contact the module developer for assistance.";
	}
}
/* End of file details.php */
