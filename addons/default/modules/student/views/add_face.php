
<?php if (validation_errors()): ?>
<div class="alert alert-error animated fadeIn">
	<button type="button" class="close" data-dismiss="alert">�~</button>
	<?php echo validation_errors(); ?>
</div>
<?php endif; ?>

<?php echo form_open_multipart(uri_string(), 'class="streams_form"'); ?>

<div class="form_inputs">

	<label for="<?php echo $fields['face']['input_slug'];?>"><?php echo $this->fields->translate_label($fields['face']['input_title']);?> <?php echo $fields['face']['required'];?></label>
	<div class="input"><?php echo $fields['face']['input']; ?></div>
	<?php if( $fields['face']['instructions'] != '' ): ?>
		<span class="help-block"><?php echo $this->fields->translate_label($fields['face']['instructions']); ?></span>
	<?php endif; ?>
	<input type="hidden" name="owner" value="<?php echo $this->current_user->id?>"/>
	
	
</div>

<div class="form-actions">
	<button type="submit" name="btnAction" value="save" class="btn btn-color"><span><?php echo lang('buttons:save'); ?></span></button>
	<a href="<?php echo site_url(isset($return) ? $return : 'admin/streams/entries/index/'.$stream->id); ?>" class="btn btn-gray"><?php echo lang('buttons:cancel'); ?></a>
</div>

<?php echo form_close();?>