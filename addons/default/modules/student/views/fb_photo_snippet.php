<style>
	.face-items{ }
	.face-item { height: 100px; float: left; margin: 5px; position: relative; }
	.face-item img { max-height: 100px }
</style>

<strong>Disclaimer.</strong><br>
<p>When you login to Facebook, this site will only ask for your personal information (name and email) and photos that is set PUBLIC. Facebook will firstly ask for your permission to do that. Carefully read the permission dialog box that is provided by Facebook to ensure yourself.</p>

<p>Your information and photos will be saved locally on this site hard drive. However, your photos will be only used as your <strong>training images for face recognition</strong> and will not be used for other purposes.</p>

<?php	if ($fb_connected){ ?>
<hr>
		<p>You are connected to Facebook using this account: <a href="<?php echo $fb_info['link'] ?>"><?php echo $fb_info['name'] ?></a></p>
		
		<p>You can revoke access to this Facebook account by clicking <a href="<?php echo base_url('student/fb/revoke') ?>">this link.</a></p>
		
<?php
if ($photo_permission){
	// Show Facebook photos preview, if exists
	if (!empty($graphObject)){
		$data = isset($graphObject['data']) ? $graphObject['data'] : $graphObject->data;
?>
<p>Live photos from your Facebook (preview only, not downloaded yet):</p>
<div class="face-items">
<?php
		// Fetch and display each images
		foreach($data as $from){
			// to show the smallest image available on image
			$n = count($from->images);
			$n = $n-1;
?>
		<div class="face-item">
			<a href="<?php echo $from->images[0]->source?>">
				<img  src='<?php echo $from->images[$n]->source?>' alt="" style="height: 100px; max-height: 100px">
			</a>
		</div>	
<?php	} ?>
	<div style="clear:both"></div>
</div>
<?php
	}else{
		echo "<span style='color: darkgray'> You do not have any public photos on your account. </span>";
	}
}else{
	// Show button for reasking the photos
?>
<p>It seems that you did not granted this webpage to access your Facebook photos yet:</p>
<a href="<?php echo base_url('student/fb/reauthorize'); ?>">Confirm Facebook Photos Permission</a>
<?php }?>
<?php
	}else{
		echo "<hr>Please login to Facebook, to fetch photos as a training image for your student account.<br><br>";
		echo "<a href='" . base_url('student/fb/authorize') ."'>Connect to Facebook</a>";
	}
?>
<hr>



<?php
if (!empty($graphObject)){
	//images paging
	$paging = isset($graphObject['paging']) ? $graphObject['paging'] : $graphObject->paging;
	if (isset($paging->previous)) $prev_url = $paging->cursors->before;
	if (isset($paging->next)) 	  $next_url = $paging->cursors->after;
	
	//create paging button
	$urlHead = site_url('student/fb/index?');
	if (isset($prev_url)) echo "<a href='{$urlHead}before={$prev_url}'>Previous</a>";
	if (isset($prev_url) && isset($next_url)) echo " | ";
	if (isset($next_url)) echo "<a href='{$urlHead}after={$next_url}'>Next</a>";
?>
<?php ?>
<hr>
<a href="<?php echo site_url('student/fb/download'); ?>">Download All Photos from this Account</a>
<?php } ?>



