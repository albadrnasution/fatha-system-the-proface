
<style>
	.face-item{	float: left; margin: 5px; position: relative; }
	.face-item.fu img {min-height:150px; min-width:150px;height:150px; width:150px;}
	.face-item:hover .face-item-tool { display: inline-block }
	.face-item .face-item-tool{ display: none; position: absolute; top: 0;
		right: 0; margin-top: 10px; margin-right: 10px;}
	.face-item-tool-btn { font-size: 0.8em !important;}

	  #progressbar {
		margin-top: 20px;
	  }
	 
	  .progress-label {
		font-weight: bold;
		text-shadow: 1px 1px 0 #fff;
	  }
	 
	  .ui-dialog-titlebar-close {
		display: none;
	  }
</style>
<?php echo Asset::render(); ?>
<?php
	// variabel for excluded face sn_photo
	$exludedFace = array();
?>
<section class="title" style="margin: 25px 0px;border-bottom: 1px solid #ECECEC;">
	<h2 style="float: left"><?php echo lang('face:list') ?></h2>
	<a class="face-item-tool-btn face-item-tool-add" style="float: left; margin: 18px;" href="<?php echo site_url("student/face/add") ?>">Add</a>
	<div style="clear: both"></div>
</section>

<section class="item">
	<?php if (!empty($entries)):
	?>
		<div class="face-items">
			<?php foreach( $entries as $item ):
				$img_link = $item['face']['image']; 
				$img_url  = site_url() . "/files/thumb/" . $item['face']['filename'] . "/200/200/fit";
				$face_info = json_decode(utf8_kill_entity_decode($item['face_info'])); 
				if (isset($face_info) && $face_info->selection!=-1){
					$img_url = base_url() . "/uploads/default/fu_photos/candidate/" . $item['owner']['id'] . "/i" . $item['id'] . "_c" . $face_info->selection . ".jpg";
				}
				$pop_link = base_url() . 'student/info/img_pop/file/' .  $item['id'] ;
			?>
			<div class="face-item fu">
				<div class="face-item-tool" >
				<?php
					echo anchor('student/face/edit/'.$item['id'], lang('face:edit'), 'class="face-item-tool-btn face-item-tool-edit"');
					echo anchor('student/face/delete/'.$item['id'], lang('face:delete'), 'class="face-item-tool-btn face-item-tool-del"');
				?>
				</div>
				<a href="<?php echo $pop_link ?>"  class="fc-up">
					<img src='<?php echo $img_url ?>' alt="{{ staff_name }}">
				</a>
			</div>
			<?php endforeach; ?>
			<div style="clear: both"></div>
			
		</div>
		
	<?php else: ?>
		<div class="no_data"><?php echo "You do not have any uploaded photos yet"; ?></div>
	<?php endif;?>
	
	<h3 style="margin-top: 40px;margin-bottom: -10px;float: left;width: 300px">Photos from Facebook</h3>
	<?php if (empty($fb_images['entries']))  : ?>
		<div style="clear:both"></div>
		<div class="no_data" style='margin-top: 50px'>You do not have any Facebook photos yet.</div
		<div class="no_data" style='margin-top: 10px'><a href="<?php echo base_url() . "student/fb"?>">Connect to Facebook here.</a></div>
	<?php else : ?>
	<button id="deleteButton" style="float: right; margin-top: 40px;">Delete All Facebook Photos </button>
	<div id="dialog-confirm" title="Empty Facebook Photos?">
		<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>These photos will be permanently deleted and cannot be recovered. Are you sure?</p>
	</div>
	<!--<a style="float: right; margin-top: 40px;   cursor: pointer;"  onclick="delete_all_fbimages()" >Delete All Facebook Photos </a> -->
	<div style="clear:both"></div>
	
	<div id="dialog" title="File Download">
	  <div class="progress-label">Preparing deletion...</div>
	  <div id="progressbar"></div>
	</div>
	<h4 style="margin-top: 40px;margin-bottom: -10px;">Included in Training Images</h3>
	<hr>
	<div class="face-items" >
			<?php foreach ($fb_images['entries'] as $fbi) :
				$photo_info = json_decode(utf8_kill_entity_decode($fbi["photo_info"]));
				
				if (property_exists($photo_info, 'exclude') and $photo_info->exclude=="yes"){
					$exludedFace[] = $fbi;
				}
				else {
					$img_link = base_url() . $fbi['photo_path'] . "original/" . $fbi['photo_owner']['id'] . "/" . $fbi['photo_id'] . ".jpg";
					$pop_link = base_url() . 'student/info/img_pop/fb/' .  $fbi['id'] ;
					$thm_link = str_replace('original', 'thumb', $img_link);
					$fce_link = FCPATH . $fbi['photo_path'] . "face/" . $fbi['photo_owner']['id'] . "/" . $fbi['photo_id'] . ".jpg";
					if (file_exists($fce_link)){
						$thm_link = str_replace('original', 'face', $img_link);
					}
			?>
			<div class="face-item" id="<?php echo "fbi-" . $fbi['id'] ?>">
				<div class="face-item-tool" >
				<?php
					echo anchor('student/face/exclude/'.$fbi['id'], lang('face:edit'), 'class="face-item-tool-btn face-item-tool-exclude"');
					echo anchor('student/face/deletefb/'.$fbi['id'], lang('face:delete'), 'class="face-item-tool-btn face-item-tool-del"');
				?>
				</div>
				<a href="<?php echo $pop_link ?>" class="fc-fb">
					<img src='<?php echo $thm_link ?>' alt="{{ staff_name }}" style="height:80px; max-height:100px" >
				</a>
			</div>
			<?php 
				}
			?>
			<?php endforeach; ?>
			<div style="clear: both"></div>
			
	</div>
	
	<h4 style="margin-top: 40px;margin-bottom: -10px;">Excluded Photos</h3>
	<hr>
	<div class="face-items" >
		<?php foreach ($exludedFace as $fbi) : 
				$img_link = base_url() . $fbi['photo_path'] . "original/" . $fbi['photo_owner']['id'] . "/" . $fbi['photo_id'] . ".jpg";
				$pop_link = base_url() . 'student/info/img_pop/fb/' .  $fbi['id'] ;
				$thm_link = str_replace('original', 'thumb', $img_link);
				$fce_link = FCPATH . $fbi['photo_path'] . "face_exclude/" . $fbi['photo_owner']['id'] . "/" . $fbi['photo_id'] . ".jpg";
				if (file_exists($fce_link)){
					$thm_link = str_replace('original', 'face_exclude', $img_link);
				}
		?>
		<div class="face-item" id="<?php echo "fbi-" . $fbi['id'] ?>">
			<div class="face-item-tool" >
			<?php
				echo anchor('student/face/exclude/'.$fbi['id']."/0", lang('face:edit'), 'class="face-item-tool-btn face-item-tool-unexclude"');
				echo anchor('student/face/deletefb/'.$fbi['id'], lang('face:delete'), 'class="face-item-tool-btn face-item-tool-del"');
			?>
			</div>
			<a href="<?php echo $pop_link ?>" class="fc-fb">
				<img src='<?php echo $thm_link ?>' alt="{{ staff_name }}" style="height: 80px; max-height:100px" >
			</a>
		</div>
		
		<?php endforeach ?>
	</div>
	<?php endif;?>
</section>
 
 
<script>
$(function() {

	$.fn.button.noConflict();
    //$( "#progressbar" ).progressbar({  value: false});
    
	$( ".face-item-tool-del" ).button({
      icons: {
        primary: "ui-icon-trash"
      },
      text: false
    });
    $( ".face-item-tool-edit" ).button({
      icons: {
        primary: "ui-icon-pencil"
      },
      text: false
    });
    $( ".face-item-tool-add" ).button({
      icons: {
        primary: "ui-icon-plusthick"
      },
      text: false
    });
	
    $( ".face-item-tool-exclude" ).button({
      icons: {
        primary: "ui-icon-arrowthick-1-s"
      },
      text: false
    });
    $( ".face-item-tool-unexclude" ).button({
      icons: {
        primary: "ui-icon-arrowthick-1-n"
      },
      text: false
    });
	
	// Pop Up
	$(".fc-up").colorbox({rel:'fc-up', transition:"none", fixed: 'true', height:"95%", innerWidth: '95%'});
	$(".fc-fb").colorbox({rel:'fc-fb', transition:"none", fixed: 'true', height:"95%", innerWidth: '95%'});
	
	var $container = $('.face-items').imagesLoaded( function() {
	  // initialize Packery after all images have loaded
		$container.packery({
		  itemSelector: '.face-item',
		  gutter: 8
		});
	});
});


$(function() {
    var  progressbar = $( "#progressbar" ),
      progressLabel = $( ".progress-label" ),
      dialogButtons = [{
        text: "Cancel Delete",
        click: cancelDelete
      }],
      dialog = $( "#dialog" ).dialog({
        autoOpen: false,
        closeOnEscape: false,
        resizable: false,
        buttons: dialogButtons,
        open: function() {
			delete_all_fbimages();
        },
        beforeClose: function() {
          deleteButton.button( "option", {
            disabled: false,
            label: "Delete Facebook Photos"
          });
        }
      }),
      deleteButton = $( "#deleteButton" )
        .button()
        .on( "click", function() {
          confirmDial.dialog( "open" );
        }),
	  confirmDial = $( "#dialog-confirm" ).dialog({
		resizable: false,
		height:240,
		modal: true,
        autoOpen: false,
		buttons: {
			"Delete all photos": function() {
				deleteButton.button( "option", {
					disabled: true,
					label: "Deleting..."
				});
				dialog.dialog( "open" );
				$( this ).dialog( "close" );
			},
			Cancel: function() {
				$( this ).dialog( "close" );
				closeDownload();
			}
		}
      });
 
    progressbar.progressbar({
      value: false,
      change: function() {
        progressLabel.text( "Current Progress: " + progressbar.progressbar( "value" ) + "%" );
      },
      complete: function() {
        progressLabel.text( "Complete!" );
        dialog.dialog( "option", "buttons", [{
          text: "Close",
          click: closeDownload
        }]);
        $(".ui-dialog button").last().focus();
      }
    });
 
    function progress(val) {
      progressbar.progressbar( "value", val );
 
    }
 
    function closeDownload() {
      dialog
        .dialog( "option", "buttons", dialogButtons )
        .dialog( "close" );
      progressbar.progressbar( "value", false );
      progressLabel.text( "Prepare deletion..." );
      deleteButton.focus();
    }
	
	function cancelDelete(){
		canceled = true;
		closeDownload();
	}
	
	var canceled = false;
	
	function delete_all_fbimages(){
		canceled=false;
		$.getJSON("<?php echo base_url().'student/face/deletefball/' ?>", function(json) {
			console.log(json);
			var nimg = json.length;
			var ndel = 0;
			json.some(function(pid) {
				if (canceled) return true;
				$.get('<?php echo base_url()?>student/face/deletefb/' + pid,function(data,status) {
					console.log("FB foto " + pid + " is deleted");
					ndel++;
					$("#fbi-"+pid).remove();
					$('.face-items').packery();
					progress(Math.round(100 * ndel / nimg));
				},'html');
				return false;
			});
		});
	}
  });

</script>

