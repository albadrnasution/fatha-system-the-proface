
<?php echo Asset::render(); ?>
<style>
  .ui-progressbar {
    position: relative;
  }
.progress-label{
	text-align:center;
    position: absolute;
    left: 50%;
    font-weight: bold;
    text-shadow: 1px 1px 0 #fff;
}
</style>

<section class="title">
	<h2><?php echo lang('class:view'); ?></h2>
</section>

<section class="item">
<div class="content">
	<div style="width: 50%; float: left">
		<table style="width: 100%">
			<tr>
				<td style="width: 20%"><label>Code:</label></td><td><?php echo $class->code ?></td>
			</tr>
			<tr>
				<td><label>Subject Name:</label></td><td><?php echo $class->name ?></td>
			</tr>
			<tr>
				<td><label>Subject Room:</label></td><td><?php echo $class->room ?></td>
			</tr>
			<tr>
				<td style="vertical-align: text-top;"><label>Students: </label></td>
				<td style="position:relative; vertical-align: text-top;">
				<?php if ($list_student['total'] > 0): ?>
				<ol style="margin-bottom: 0px">
				<?php
				foreach($list_student['entries'] as $item){?>
					<li><a href="<?php echo site_url() . "/user/" . $item['student']['id']  ?>"><?php echo $item['student']["first_name"].' '.$item['student']["last_name"] ?></a></li>
				<?php } ?>
				</ol>			
				<?php else: ?>
					<span style="font-style:italic"><?php echo lang('class:no_student'); ?></span>
				<?php endif;?>
				</td>
			</tr>
		</table>
	</div>
	<div style="width: 48%;float: left">
		<div><label>Your Attendance:</label>
			<div id="attendance_bar" style="font-size: 0.75em"><div class="progress-label"><?php echo $my_att['attendance'] ?>%</div></div>
		</div>
		<div style="margin-top: 10px;">
			<label>Augmentation Comment:</label>
		</div>
		<form action="<?php echo site_url("student/class/update_cmt/")?>" method="post">
			<textarea id="resizable" name = "comment" style="width: 100%"><?php echo $my_att['comment']?></textarea>
			<input type="hidden" name="id" value="<?php echo $my_att['id']?>"/>
			<input type="hidden" name="class_id" value="<?php echo $class->id?>"/>
			<input id='submit' type='submit' name = 'submit' class="btn_save" value = 'Update Comment' style="font-size: 0.8em;">
		</form>
		
	</div>
	<div style="clear: both"></div>
</div>

<script>
	$( "#attendance_bar" ).progressbar({
      value: <?php echo $my_att['attendance'] ?>
    });
	$( ".btn_save" ).button();
</script>
