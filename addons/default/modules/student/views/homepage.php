<p>Welcome to student face repository. This site is an webpage mock-up of Student Management System to emulate DreamCampus. This site is a part of bigger component to achive teaching aid system using face recognition.&nbsp;<br />
<br />
Basically this site has some functions:<br />
- Manage students and classes/subject they take<br />
- Manage image uploaded by students as a face training set<br />
- Manage social network connection (Facebook) to fetch photos also for face training set<br />
- Connect with a Server Program (that handle face recognition) and also Tablet Devices&nbsp;(that handle input for recognition and info augmentation)</p>

<a href="http://bee00.aisl.cs.tut.ac.jp/web/index.php/users/login/users">Press here to login</a>.

<?php


?>