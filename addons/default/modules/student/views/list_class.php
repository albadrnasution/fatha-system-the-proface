
<section class="title" style="margin: 25px 0px;border-bottom: 1px solid #ECECEC;">
	<h2><?php echo lang('attendance:list') ?></h2>
</section>

<section class="item">
	You are registered to these classes:
	<?php if (!empty($entries)):  ?>
		<div class="class-items">
			<ul>
			<?php foreach( $entries as $item ):?>
				<li><a href="<?php echo site_url("student/class/view/" . $item['class']['id']) ?>"><?php echo $item['class']['name'] ?></a> <?php echo ' ('. $item['class']['room']  . ')'				?></li>
			<?php endforeach; ?>
			</ul>
		</div>
		
	<?php else: ?>
		<div class="no_data"><?php echo lang('sample:no_items'); ?></div>
	<?php endif;?>
	
</section>