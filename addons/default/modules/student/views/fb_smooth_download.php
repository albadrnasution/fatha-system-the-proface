<style>
div.clr{
	clear: both;
}
.face-items{ }
.face-item { height: 80px; float: left; margin: 5px; position: relative; }
.face-item img { height: 80px; max-height: 80px }
</style>

<strong>Disclaimer.</strong><br>
<p>When you login to Facebook, this site will only ask for your personal information (name and email) and photos that is set PUBLIC. Facebook will firstly ask for your permission to do that. Carefully read the permission dialog box that is provided by Facebook to ensure yourself.</p>

<p>Your information and photos will be saved locally on this site hard drive. However, your photos will be only used as your <strong>training images for face recognition</strong> and will not be used for other purposes.</p>

<hr>
Web is downloading your photos. Please wait... You can also stop the download by closing this page. 
<div id="meta"> </div><hr>
<div id="meta2"> </div><hr>
<div id="images"><div class="clr"></div></div>
<div id="dump"></div>

<script>
	jQuery( document ).ready(function(){
		//the first batch of photos
		var photos = <?php echo json_encode($photos) ?>;
		getDownBatch(downBatchUrl);
		//Test for Null: + "TWpFNE16WXpPVEV5TURFeE56b3hNekV3TWpjMk5EQXpPak01TkRBNE9UWTBNRFkwTnpnek5nPT0ZD"
		
		// Starting downloading images by getting fb object with ddlPoint function
		//oldDDL one by one
		//getFBObject(ddlPoint);
		
		//dummy for last 3 images albadr 
		//getFBObject(ddlPoint + "TWpZNE5qY3lPRE01T0RFNU9UVXpPakV6TVRZME56TTNPRFU2TXprME1EZzVOalF3TmpRM09ETTI=");
	});
	
	var ddlPoint = '<?php echo site_url("student/fb/ddl?after=") ?>';
	var downBatchUrl = '<?php echo site_url("student/fb/downbatch?after=") ?>';
	var downOneUrl = '<?php echo site_url("student/fb/downone") ?>';
	var ndownloaded = 0;
	
	function getFBObject(jsonUrl){
		console.log(jsonUrl);
		$.getJSON(jsonUrl, function(json) {
			console.log(json);
			if (json.url != "finished"){
				// not finished yet, continue for next
				console.log("Tag point: " + json.info.x + " " + json.info.y);
				ndownloaded++;
				// add to div for display
				$("#images").prepend(buildDivDldImage(json));
				$("#meta").html(ndownloaded + " images are downloaded");
				// next images
				getFBObject(ddlPoint + json.paging.cursors.after);
				
				// pass json to the facedet server program
				if (conn){
					delete json.paging;
					conn.send(JSON.stringify(json));
				}
			}else{
				// no next images, download finished
				alert('Download Finished');
			}
		});
	}
	
	var countUpdate = 0;
	function getDownBatch(jsonUrl){
		var ndled = 0;
		console.log(jsonUrl);
		$.getJSON(jsonUrl, function(allPhotos) {
			console.log(allPhotos);
			var photos = allPhotos.photos;
			var nphoto = photos.length;
			
			if (photos != null){
				photos.forEach(function(entry) {
					setTimeout(function(){
						console.log("Sending request to download " + entry.id);
						// download one by one
						$.getJSON(downOneUrl +"/"+entry.id, function(onePhotoJson) {
							console.log(onePhotoJson);
							// not finished yet, continue for next
							console.log("Tag point: " + onePhotoJson.info.x + " " + onePhotoJson.info.y);
							ndled++;
							var dbid = onePhotoJson.dbid;
							// add to div for display
							$("#images").prepend(buildDivDldImage(onePhotoJson));
							$("#meta").html(ndled + " images of " + nphoto +" are downloaded");
							
							$.post( "<?php echo site_url() . "/student/fb/reqserver_fbimg/"?>", { reqserv_info: onePhotoJson, slug: 'fb' })
								.done(function( dataret ) {
									if (dataret == dbid){
										countUpdate++;
										console.log('Face update succeed, dbid=' + dbid);
										if (countUpdate==ndled){
											$("#meta2").html("Download Finished! Please go to Faces tab and select which face for training images.");
											alert('Download Finished.');
										}
									}
							});
							
							if (nphoto == ndled){
								// no next images, download finished
								$("#meta2").html("Now detecting face for the downloaded photos...");
							}
						});
					}, 1000);
				});
			}else{
				alert("No photos available for download");
			}
			
		});
		
	}
	
	function buildDivDldImage(json){
		var div = "<div class='face-item'>";
		div += "<div class='dld-res'><img src='"+json.info.picture+"'/></div>";
		div += "</div>";
		return div;
	}
	
	
	// var conn = new WebSocket('<?php echo $this->config->item('pf_server_url');?>');
	// conn.onopen = function(e) {
		// console.log("Connection established!");
	// };
	// conn.onmessage = function(e) {
		// console.log(e.data);
		
		// $.post( "<?php echo site_url() . "/student/face/update/"?>", { info: e.data, slug: 'fb' })
			// .done(function( data ) {
				// if (data == ""){}
			// });
	// };
</script>

