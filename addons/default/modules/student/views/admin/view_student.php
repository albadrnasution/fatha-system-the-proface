<?php echo Asset::render(); ?>
<section class="title">
  <h4>
    <?php echo user_displayname($student->id) ?>
  </h4>
</section>
<section class="item">
  <div class="content">
    <table>
      <tbody>
        <tr>
          <td style="width:20%">
            <strong>E-mail:</strong>
          </td>
          <td><?php echo $student->email ?></td>
        </tr>
        <tr>
          <td>
            <strong>Username:</strong>
          </td>
          <td><?php echo $student->username ?></td>
        </tr>
        <tr>
          <td>
            <strong>Name:</strong>
          </td>
          <td><?php echo $student->first_name . ' ' . $student->last_name  ?></td>
        </tr>
        <tr>
          <td>
            <strong>Date of Birth:</strong>
          </td>
          <td>January 1, 1970</td>
        </tr>
        <tr>
          <td>
            <strong>Gender:</strong>
          </td>
          <td><?php echo $student->gender == "m" ? "Male" : "Female" ?></td>
        </tr>
        <tr>
          <td>
            <strong>Phone:</strong>
          </td>
          <td><?php echo $student->phone ?></td>
        </tr>
      </tbody>
    </table>
	
	<hr>
	<h4> Classes: </h4>
	
	<?php if (!empty($classes['entries'] )):  ?>
		<div class="class-items">
			<ol>
			<?php foreach( $classes['entries'] as $item ):?>
				<li><a href="<?php echo site_url().'/student/admin/class/view/'.$item['class']['id'] ?>"><?php echo $item['class']['name'] ?> </a> <?php echo '('.$item['class']['room'].')' ?></li>
			<?php endforeach; ?>
			</ol>
		</div>
		
	<?php else: ?>
		<div class="no_data"><?php echo lang('sample:no_items'); ?></div>
	<?php endif;?>
	
	
	<hr>
	<h4> Faces: </h4>
	
	<?php if (!empty($faces['entries'])):  ?>
		<div>
			<?php foreach( $faces['entries'] as $item ):?>
			<div style="float: left; margin: 0 5px; line-height: 100px">
			<a href="<?php echo $item['face']['image']; ?>">
				<img src='<?php echo site_url() ?>/files/thumb/<?php echo $item['face']['filename'];  ?>/100/100/fit' alt="{{ staff_name }}">
			</a>
			</div>
			<?php endforeach; ?>
			<div style="clear: both"></div>
			
		</div>
		
	<?php else: ?>
		<div class="no_data"><?php echo lang('sample:no_items'); ?></div>
	<?php endif;?>
	
  </div>
</section>
<script>
$(function() { //$.fn.button.noConflict(); $(
"#btn_add_student" ).button({ icons: { primary: "ui-icon-plusthick"
}, text: true }); });
</script>
