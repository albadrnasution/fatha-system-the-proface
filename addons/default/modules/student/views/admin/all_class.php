<section class="title">
	<h4><?php echo lang('class:list'); ?></h4>
</section>

<section class="item">
<div class="content">

	<?php if ($classes['total'] > 0): ?>
	
		<table class="table" cellpadding="0" cellspacing="0">
			<thead>
				<tr>
					<th><?php echo lang('class:code'); ?></th>
					<th><?php echo lang('class:name'); ?></th>
					<th><?php echo lang('class:room'); ?></th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($classes['entries'] as $cls): ?>
				<?php
					//check whether current user has assigned as teacher on class $cls
				    $params = array();
					$params['stream'] = 'teacher';
					$params['namespace'] = 'aisl';
					$params['where'] = "class_ref={$cls['id']}&&teacher_ref={$this->current_user->id}";
					$list_teacher = $this->streams->entries->get_entries($params);
					if ($list_teacher['total'] > 0 || $this->current_user->group_id == 1) :
				?>
				<tr>
					<td><?php echo $cls['code']; ?></td>
					<td><?php echo $cls['name']; ?></td>
					<td><?php echo $cls['room']; ?></td>
					<td class="actions"><?php echo anchor('student/admin/class/view/' . $cls['id'], lang('global:view'), 'class="button edit"'); ?>
					<?php echo anchor('student/admin/class/edit/' . $cls['id'], lang('global:edit'), 'class="button edit"'); ?>
					<?php if ($this->current_user->group_id == 1) : ?>
                    <?php echo anchor('student/admin/class/delete/' . $cls['id'], lang('global:delete'), array('class' => 'confirm button delete')); ?>
					<?php endif ?>
					</td>
				</tr>
				<?php endif; ?>
				<?php endforeach; ?>
			</tbody>
		</table>
		
		<?php echo $classes['pagination']; ?>
		
	<?php else: ?>
		<div class="no_data"><?php echo lang('class:no_class'); ?></div>
	<?php endif;?>
	
</div>
</section>