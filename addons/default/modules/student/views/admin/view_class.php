
<?php echo Asset::render(); ?>
<style>
@media (min-width: 800px) {
	.half_div{	width: 50%; float:left;  }
	.half_div:first-child{padding-right: 20px; box-sizing: border-box}
}
td.actions{padding: 5px 10px}
td.actions a.ui-button{margin-top: 0px; height: 24px;}
#teacher_list .btn_del{
	display: none; height: 16px; vertical-align: text-bottom; margin-left: 25px;margin-top: 0px;
}
#teacher_list li:hover .btn_del{
	display: inline-block;
}
</style>

<section class="title">
	<h4><?php echo $class->name ?></h4>
</section>

<section class="item">
<div class="content">
<div class="half_div">
	<h4>General Information</h4>
	<table>
		<tr>
			<td style="width: 20%"><label>Code:</label></td><td><?php echo $class->code ?></td>
		</tr>
		<tr>
			<td><label>Subject Name:</label></td><td><?php echo $class->name ?></td>
		</tr>
		<tr>
			<td><label>Subject Room:</label></td><td><?php echo $class->room ?></td>
		</tr>
		<tr>
			<td style="vertical-align: text-top;"><label>Teacher: </label></td>
			<td style="position:relative; vertical-align: text-top;">
			<?php if ($list_teacher['total'] > 0): ?>
			<ol id="teacher_list" style="margin-bottom: 0px">
			<?php
			foreach($list_teacher['entries'] as $item){?>
				<li><a href="<?php echo site_url() . "/user/" . $item['teacher_ref']['id']  ?>"><?php echo $item['teacher_ref']["first_name"].' '.$item['teacher_ref']["last_name"] ?></a>
				<?php if ($this->current_user->group_id == 1) : ?>
				<a href='<?php echo site_url()."student/admin/attendance/delete_teacher/" .$item['id']."?cid=".$class->id?>' class='btn gray btn_del' >Delete</a>
				<?php endif; ?>
				</li>
			<?php } ?>
			</ol>			
			<?php else: ?>
				<span style="font-style:italic"><?php echo lang('class:no_teacher'); ?></span>
			<?php endif;?>
			
			<?php if ($this->current_user->group_id == 1) : ?>
			<br>
			<a id="btn_add_teacher" class="btn_add" onclick="$('#add_teacher').show();$('#btn_add_teacher').hide();" style="margin-top: 5px;">Add Teacher</a>
			<div id="add_teacher" style="display: none">
				<?php echo form_open(site_url() . "student/admin/class/assign_teacher", 'class="streams_form"'); 
					if (empty($user_teacher)) echo "All teachers are assigned in this class.";
					else {
				?>
				<select name="teacher_id">
					<?php foreach ($user_teacher as $ut){
						echo "<option value='{$ut->id}'>{$ut->display_name}</option>";
					}
					?>
				</select>
				<input type="hidden" name="class_id" value="<?php echo $class->id; ?>">
				<button type="submit" class="btn blue" style="vertical-align: top"><?php echo lang('buttons:save'); ?></button>
				<a onclick="$('#add_teacher').hide();$('#btn_add_teacher').show();" class="btn gray"  style="vertical-align: top"><?php echo lang('buttons:cancel'); ?></a>
					<?php } ?>
				<?php echo form_close();?>
			</div>
			<?php endif;?>
			</td>
		</tr>
	</table>
		
	<div>
	<h4>Face Model Status</h4>
	<?php
		if (empty($class->class_info)){
			echo "<i>Not trained yet</i><br><br>";
		}else{
			//convert info (json) to object
			$class_info = json_decode ($class->class_info);
			//print in easy to read manner
			$training_infos = $class_info->training;
			echo "<table>";
			echo "<tr><td>Date</td><td>Students Count</td><td>Images Count</td><td>Each student's image</td></tr>";
			foreach($training_infos as $ti){
				echo "<tr>";
				echo "<td>{$ti->status}</td><td>{$ti->number_of_students}</td><td>{$ti->number_of_images}</td><td>" ;
				foreach($ti->number_of_im_per_st as $imst){
					echo "<a href='" . site_url() ."admin/student/view/{$imst->sid}'>{$imst->sid}</a> ({$imst->nim}) ";
				}
				echo "</td></tr>";
			}
			echo "</table>";
		}
	?>
	<a href="<?php echo site_url() . "/student/admin/class/train_fm/" . $class->id  ?>" class="btn btn-color orange"><span>Train Face Model</span></a>
	</div>
</div>

<div class="half_div">
	<h4>Students Detail</h4>
	<table >
		<?php if ($list_student['total'] > 0): ?>
		<thead><tr><td>ID</td><td style="width:20%">Name</td><td>Latest Comment</td><td>Attendance</td><td style="width:75px"></td></tr></thead>
		<tbody>
			<?php
			foreach($list_student['entries'] as $item){
			    $display_name = user_displayname($item['student']['id']);
				echo "<tr>";
				echo "<td>{$item['student']['id']}</td>";
				echo "<td><a href='" . site_url() . "admin/student/view/" . $item['student']['id'] . "'>{$display_name}</a></td>";
				echo "<td><i>{$item['comment']}</i></td>";
				echo "<td>{$item['attendance']}%</td>";
				echo "<td class='actions'>";
				echo "<a href='". site_url() ."admin/student/attendance/delete/{$item['id']}?cid={$class->id}' class='btn gray btn_del'>Delete</a>";
				echo "<a href='". site_url() ."admin/student/attendance/edit/{$item['id']}/{$class->id}' class='btn gray btn_edit'>Edit Attendance</a>";
				echo "</td></tr>";
			}
			?>
		</tbody>
		<?php else: ?>
			<span style="font-style:italic"><?php echo lang('class:no_student'); ?></span>
		<?php endif;?>
	</table>
	
	<a id="btn_add_student" class="btn_add" onclick="return show_formAddStudent()" style="margin-top: 5px;">Add Student</a>
	<div id="form-add-student" style="padding: 20px; display: none">
		<?php echo form_open(site_url() . "student/admin/class/assign_student", 'class="streams_form"'); 
			if (empty($user_student)) echo "All students are in this class.";
			else {
		?>
			<select name="student_id">
				<?php foreach ($user_student as $ut){
					echo "<option value='{$ut->id}'>{$ut->display_name}</option>";
				}
				?>
			</select>
			<input type="hidden" name="class_id" value="<?php echo $class->id; ?>">
			<button type="submit" class="btn blue" style="vertical-align: top"><?php echo lang('buttons:save'); ?></button>
			<a onclick="return hide_formAddStudent()" class="btn gray"  style="vertical-align: top"><?php echo lang('buttons:cancel'); ?></a>
			<?php } ?>
		<?php echo form_close();?>
	</div>

	
</div>
<div style="clear:both"></div>
</div>

</section>

<script>
function show_formAddStudent(){
	$("#form-add-student").show()
	$("#btn_add_student").hide()
    return false;
}
function hide_formAddStudent(){
	$("#form-add-student").hide()
	$("#btn_add_student").show()
    return false;
}

$(function() {

	//$.fn.button.noConflict();
    $( ".btn_add" ).button({
      icons: {
        primary: "ui-icon-plusthick"
      },
      text: true
    });
	$( ".btn_edit" ).button({
      icons: {
        primary: "ui-icon-pencil"
      },
      text: false
    });
	$( ".btn_del" ).button({
      icons: {
        primary: "ui-icon-trash"
      },
      text: false
    });
});

</script>