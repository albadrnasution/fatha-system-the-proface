<section class="title">
	<h4>Training face model of class "<?php echo $class->code . ": " . $class->name?>"</h4>
</section>

<section class="item">
<div style="padding: 20px" id="mylog">
	Processing is started...<br>
	Contacting server...<br>
</div>
</section>
<script>	
	var conn = new WebSocket('<?php echo $this->config->item('pf_server_url');?>');
	conn.onopen = function(e) {
		console.log("Connection established!");
		conn.send(json);
	};
	
	conn.onmessage = function(e) {
		console.log(e.data);
		var obj = JSON.parse(e.data);
		
		if (obj.success=="yes"){
			$("#mylog").append("Sucessfully training. <br>");
			$("#mylog").append("Saving info and redirecting... <br>");
			var ti = JSON.stringify(obj.training);
			// saving training info			
			$.post( "<?php echo site_url() . "student/admin/class/update_info/" . $class->id?>", { training_info: ti })
				.done(function( data ) {
					console.log("Saving return:" + data);
					if (data == "updated"){
						// redirecting
						window.location =  "<?php echo site_url() . "student/admin/class/view/" . $class->id ?>"
					}else{
						$("#mylog").append("Error when saving training info! <br>");
					}
			});
		}else{
			$("#mylog").append("Training failed! <br>");
			$("#mylog").append("<a href=''>Try Again?</a> <br>");
		}
	};
	
	conn.onerror = function(e) {
		$("#mylog").append("The following error occurred: " + e.data + "<br>")
	}

	var json = '<?php echo json_encode($json) ?>';
	
	jQuery( document ).ready(function(){});
</script>