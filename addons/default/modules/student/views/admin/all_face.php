
<?php echo Asset::render(); ?>
<style>
	.face-item{	float: left; margin: 5px; position: relative;}
	.face-item img {height: 100px; width: 100px}
	.face-name{	height: 100px; width: 100px; text-align: center;	justify-content: center;
		display: -webkit-box;display: -webkit-flex;display: -moz-box;display: -ms-flexbox;display: flex;
		-webkit-flex-align: center;-ms-flex-align: center;-webkit-align-items: center;align-items: center;
	}
	.face-item:hover .face-item-tool { display: inline-block }
	.face-item .face-item-tool{ display: none; position: absolute; top: 0;
		right: 0; margin-top: 10px; margin-right: 10px;}
	.face-item-tool-btn { font-size: 0.8em !important;}
</style>

<?php 
	$temp_img_user = null;
?>

<section class="title">
	<h4><?php echo lang('face:list') ?></h4>
</section>

<section class="item">
	<div class="" style="padding: 10px;"> 
	<?php echo form_open('admin/sample/delete');?>
	
	<?php if (!empty($entries)):  ?>
		<div>
			<?php foreach( $entries as $item ):
				
				$img_url  = site_url() . "/files/thumb/" . $item['face']['filename'] . "/100/100/fit";
				$face_info = json_decode(utf8_kill_entity_decode($item['face_info'])); 
				if (isset($face_info) && $face_info->selection!=-1){
					$img_url = base_url() . "/uploads/default/fu_photos/candidate/" . $item['owner']['id'] . "/i" . $item['id'] . "_c" . $face_info->selection . ".jpg";
				}
				$pop_link = base_url() . 'student/info/img_pop/file/' .  $item['id'] ;
			?>
			
			<?php if ($temp_img_user!=$item['owner']['id']){
						$temp_img_user=$item['owner']['id'];
			?>
				<div class="face-item face-name">
						<a href="<?php echo site_url("admin/student/view/" . $item['owner']['id'])?>"><?php echo $item['owner']['first_name'] .  " " . $item['owner']['last_name'];?></a>
				</div>
			<?php } ?>
			<div class="face-item" >
				<div class="face-item-tool" >
				<?php
					echo anchor('admin/student/edit/'.$item['id'], lang('face:edit'), 'class="face-item-tool-btn face-item-tool-edit"');
					echo anchor('admin/student/delete/'.$item['id'], lang('face:delete'), 'class="face-item-tool-btn face-item-tool-del"');
				?>
				</div>
				<a href="<?php echo $pop_link;//$item['face']['image']; ?>" class="fc-up">
					<img src='<?php echo $img_url ?>' alt="{{ staff_name }}">
				</a>
			</div>
			<?php endforeach; ?>
			<div style="clear: both"></div>
			
		</div>
		
	<?php else: ?>
		<div class="no_data"><?php echo lang('sample:no_items'); ?></div>
	<?php endif;?>
	
	<?php echo form_close(); ?>
	</div>
</section>


<script>
$(function() {
	// Pop Up
	$(".fc-up").colorbox({rel:'fc-up', transition:"none", fixed: 'true', height:"95%", innerWidth: '95%'});

	//$.fn.button.noConflict();
    $( ".face-item-tool-edit" ).button({
      icons: {
        primary: "ui-icon-pencil"
      },
      text: false
    });

    $( ".face-item-tool-del" ).button({
      icons: {
        primary: "ui-icon-trash"
      },
      text: false
    });
});

</script>
