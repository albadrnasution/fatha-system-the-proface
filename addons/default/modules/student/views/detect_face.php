<?php if (count($face_cds->candidates)>0) {?>
<div style="margin: 15px 0px;" id="inbox">Select your face from detected candidates...</div>
<?php } else { ?>
<div style="margin: 15px 0px;" id="inbox">Sorry. We could not detect face from this photo...</div>
<?php } ?>
<div style="margin: 15px 0px;" id="erbox"></div>

<?php
	// Settings of Red Tag on Owner Face
	if (property_exists ($img_info, 'my_tag') && $img_info->my_tag!=null){
		$x_percent = $img_info->my_tag->x;
		$y_percent = $img_info->my_tag->y;
		$pos = "left: {$x_percent}%; top:{$y_percent}%;";
	}else{
		$pos = "display: none";
	}
	
	// Settings of Face Candidates
	if (isset($face_cds)){
		echo $fcds_setting = "";
	}else{
		$fcds_setting = "display: none";
	}
?>
<style>
	.fcd{
		position: absolute; cursor: pointer; margin-top: 0px; background: transparent; 
		-webkit-border-radius: 3px; border-radius: 3px; 
		border: 3px solid rgba(255,255,255,0.1);  
	}
	.fcd.selected{
		border: 3px solid rgba(0,255,0,0.5);  
	}
	.fcd:hover{
		border: 3px solid rgba(255,255,255,0.7);  
	}
	.fcd.selected:hover{
		border: 3px solid rgba(0,255,0,0.9);  
	}
</style>


<div class="popupWrapper" style="position: relative; max-width:100%; width:<?php echo $img_info->width?>px">
	<img class="popupPhoto" src="<?php echo $img_path ?>" style="margin-top: 0px; max-width:100%; width:<?php echo $img_info->width?>"/>
	<div class="popupInfo" style="position: absolute;width: 10px;height: 10px;background:#f00; border-radius: 100%; <?php echo $pos ?>"></div>
	<div class="facesCds" style="<?php echo $fcds_setting ?>">
	<?php 
	$i = -1;
	foreach($face_cds->candidates as $face_candidate) :
		$i++;
		$cd_width = ($face_candidate->brx - $face_candidate->tlx) ;
		$cd_height = ($face_candidate->bry - $face_candidate->tly);
		$cd_tlx = $face_candidate->tly * $img_info->width / 100;
		$cd_tly = $face_candidate->tly * $img_info->height / 100;
		$setting = "width: {$cd_width}%; height: {$cd_height}%; top: {$face_candidate->tly}%; left: {$face_candidate->tlx}%";
		$class_selected = "";
		if ($i == $face_cds->selection){
			$class_selected = "selected";	
		}
	?>
		<div class="fcd <?php echo $class_selected?>" style="<?php echo $setting?>" onclick="updateFaceCandidate(this, <?php echo $i ?>)"> </div>
	<?php	endforeach; ?>
	</div>
</div>	

<script>
$(function() {
	var screenWidth = window.screen.width, screenHeight = window.screen.height;
});

function updateFaceCandidate(obj_box, face_index){
	$.post("<?php echo site_url() . "/student/face/select/" . $img_id ?>/" + face_index, function(data) {
		console.log("Server Data: " + data);
		if (data == "<?php echo $img_id ?>"){
			alert("Update Succeed.");
			$('.fcd').removeClass('selected');
			$(obj_box).addClass('selected');
		}else{
			alert("Something Wrong");
		}
	});
}

</script>


<img src="<?php echo $img->face['image'] ?>" style="max-width: 480px; max-height: 360px" />